PRÁCTICA DISEÑO DE SOFTWARE - 2014/2015
=======================================

Grupo 03:

* Samuel Alfageme Sainz
* Pablo Carrascal Muñoz
* Andrea Escribano García

** INSTRUCCIONES PARA LA EJECUCIÓN **
-------------------------------------

### Creación de la Base de Datos:

Para crear la BDD que emplea la aplicación, se puede ejecutar el script de Apache ij que se incluye: `aplicacion/bd/scriptSQL.sql`, para ello, con `derbytools.jar` en el CLASSPATH de nuestro sistema ejecutamos:

```
java -Djdbc.drivers=org.apache.derby.jdbc.EmbeddedDriver
    org.apache.derby.tools.ij scriptSQL.sql
```

### Ejecución de la aplicación:

Cualquiera de las vistas puede ser lanzada independientemente desde netbeans pues tienen métodos main propios, el flujo de la aplicación puede verse si se ejecuta la `VistaLogin.java` y se introduce alguno de los logins que aparecen en la BDD de la aplicación, estos son:

* Responsable de Contabilidad
	* usuario: *samalfa* 
	* pass: *1234*

* Encargado de Pedidos
	* usuario: *pabcarr* 
	* pass: *2345*

* Encargado de Almacén
	* usuario: *andescr* 
	* pass: *3456*

Estructura de Carpetas del proyecto Astah:
------------------------------------------

* **Análisis** : Información previa proporcionada en el enunciado de la práctica:
	* Modelo de CU: Diagrama + CU + Actores
	* Modelo estático de Dominio (proporcionado, sin operaciones)
	* **Modelo dinámico: DSS - Descripción de los CU traducidos a operaciones de un actor contra el Sistema** - (con/sin escenarios alternativos)
* **Diseño (Arquitectura)**
	* Modelo arquitectonico en **CAPAS** - 
	* 1 modelo detallado de la arquitectura de cada capa
	* Diagramas de Arquitectura Lógica: 1/Caso de Uso + 1 Sistema
* **Realización en Diseño de los CU** : 1 por CU - extender el DSS de Análisis
* Modelo Relacional (paquete ER de astah) a partir del cual hemos generado el script SQL