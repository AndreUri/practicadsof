package modelo;

import datos.GestorPersistenciaPedido;
import java.util.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.json.JsonArray;
import javax.json.JsonObject;
import utilidades.Fechas;

/**
 * 
 * @author grupo 03 - Diseno de Software
 */
public class Factura {
    
    private int numeroFactura;
    private Date fechaEmision;
    private double importe;
    private EstadoFactura estado;
    private Date fechaPago; //[0..1]
    private String idExtractoBancario;
  

    public Factura(int numeroFactura, Date fechaEmision, double importe, EstadoFactura estado, Date fechaPago, String idExtractoBancario) {
        this.numeroFactura = numeroFactura;
        this.fechaEmision = fechaEmision;
        this.importe = importe;
        this.estado = estado;
        this.fechaPago = fechaPago;
        this.idExtractoBancario = idExtractoBancario;
    }

    public int getNumeroFactura() {
        return numeroFactura;
    }

    public Date getFechaEmision() {
        return fechaEmision;
    }

    public double getImporte() {
        return importe;
    }

    public EstadoFactura getEstado() {
        return estado;
    }

    public Date getFechaPago() {
        return fechaPago;
    }

    public String getIdExtractoBancario() {
        return idExtractoBancario;
    }

    public void setIdExtractoBancario(String idExtractoBancario) {
        this.idExtractoBancario = idExtractoBancario;
    }
    
    public static List<Factura> getFacturasVencidasAnteriores(Date fecha)
            throws SQLException {
        
        JsonArray facturasJson = GestorPersistenciaPedido.getFacturasVencidasPreviasAFecha(Fechas.getSQLDate(fecha));
        
        List<Factura> facturas = new ArrayList<>();
        
        for(int i=0 ; i<facturasJson.size() ; i++ )
            facturas.add(reconstruirFactura(facturasJson.getJsonObject(i)));
        
        return facturas.isEmpty() ? null : facturas;
    }
    
    private static Factura reconstruirFactura(JsonObject factura){
        
        int numero           = factura.getInt("numeroFactura");
        Date fechaEmision    = Fechas.stringToDate(factura.getString("fechaEmision"));
        double importe       = factura.getJsonNumber("importe").doubleValue();
        EstadoFactura estado = EstadoFactura.getEstadoAsociado(factura.getString("estado"));
        Date fechaPago       = factura.isNull("fechaPago") ? null : Fechas.stringToDate(factura.getString("fechaPago"));
        String idExtracto    = factura.getString("idExtractoBancario");
        
        return new Factura(numero,fechaEmision,importe,estado,fechaPago,idExtracto);
    }
    
    public static List<Pedido> getPedidosAsociados(int numeroFactura)
            throws SQLException {
        return Pedido.getListaPedidos(
                GestorPersistenciaPedido.getIdsPedidosAsociadosAFactura(numeroFactura));
    }
    
    public static Abonado getAbonado(int numeroFactura)
            throws SQLException {
        return getPedidosAsociados(numeroFactura).get(0).getAbonado();
    }
}
