package modelo;

import datos.GestorPersistenciaCompra;
import java.util.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.json.JsonArray;
import javax.json.JsonObject;
import utilidades.Fechas;
import utilidades.excepciones.CompraException;

/**
 * 
 * @author grupo 03 - Diseno de Software
 */
public class Compra {
    
    private final int idCompra;
    private final Date fechaInicioCompra;
    private boolean recibidaCompleta;
    private double importe;
    private boolean pagada;
    private Date fechaCompraCompletada = null;
    private Date fechaPago = null;
    private List<LineaCompra> lineasCompra = new ArrayList<>();
    
    private Compra(int idCompra, Date fechaInicioCompra, boolean recibidaCompleta, double importe, boolean pagada) {
        this.idCompra = idCompra;
        this.fechaInicioCompra = fechaInicioCompra;
        this.recibidaCompleta = recibidaCompleta;
        this.importe = importe;
        this.pagada = pagada;
    }


    public int getIdCompra() {
        return idCompra;
    }

    public Date getFechaInicioCompra() {
        return fechaInicioCompra;
    }

    public boolean isRecibidaCompleta() {
        return recibidaCompleta;
    }

    private void setRecibidaCompleta(boolean recibidaCompleta) {
        this.recibidaCompleta = recibidaCompleta;
    }

    public Date getFechaCompraCompletada() {
        return fechaCompraCompletada;
    }

    public void setFechaCompraCompletada(Date fechaCompraCompletada) {
        this.fechaCompraCompletada = fechaCompraCompletada;
    }

    public double getImporte() {
        return importe;
    }

    public boolean isPagada() {
        return pagada;
    }

    public Date getFechaPago() {
        return fechaPago;
    }
    
    private void setPagada(boolean pagada) {
        this.pagada = pagada;
    }

    private void setFechaPago(Date fechaPago) {
        this.fechaPago = fechaPago;
    }

    public List<LineaCompra> getLineasCompra() {
        return lineasCompra;
    }
    
    public void recibirLinea(int idLinea) {
        for(LineaCompra l : lineasCompra){
            if(l.getId() == idLinea)
                l.recibir();
        }
        if(todasLasLineasRecibidas())
            completar();
    }
    
    public List<LineaCompra> getLineasCompraNoRecibidas() {
        List<LineaCompra> lineasNoRecibidas = new ArrayList<>();
        for(LineaCompra l : lineasCompra)
            if(!l.isRecibida()) lineasNoRecibidas.add(l);
        return lineasNoRecibidas;
    }
    
    public void completar(){
        if(!recibidaCompleta){
            setRecibidaCompleta(true);
            setFechaCompraCompletada(Fechas.getCurrentDate());
        }
    }
    
    public boolean todasLasLineasRecibidas(){        
        return getLineasCompraNoRecibidas().isEmpty();
    }
    
    public Bodega getBodega()
            throws SQLException {
        // TODO: Violando Demeter:
        return Bodega.getBodega(Vino.getVino(lineasCompra.get(0).getLineasPedido().get(0).getReferencia()));
    }
    
    public void completarEnPersistencia()
            throws SQLException {
        for(LineaCompra l : lineasCompra)
                l.completarEnPersistencia();
         // Contar con no sobreescribir la fecha de recepcion:
        if(recibidaCompleta)
            GestorPersistenciaCompra.completarCompra(idCompra, Fechas.getSQLDate(fechaCompraCompletada));
    }
    
    public static boolean completada(int idCompra)
            throws SQLException, CompraException {
        return GestorPersistenciaCompra.comprobarCompraCompletada(idCompra);
    }
    
    public static Compra getCompra(int idCompra)
            throws SQLException {
        
        JsonObject jsonCompra = GestorPersistenciaCompra.getCompra(idCompra);
        
        Date fechaInicioCompra = Fechas.stringToDate(jsonCompra.getString("fechaInicioCompra"));
        boolean recibidaCompleta = jsonCompra.getBoolean("recibidaCompleta");
        double importe = jsonCompra.getJsonNumber("importe").doubleValue();
        boolean pagada = jsonCompra.getBoolean("pagada");

        Compra compra = new Compra(idCompra,fechaInicioCompra,recibidaCompleta,importe,pagada);
        
        if(recibidaCompleta)
            compra.setFechaCompraCompletada(Fechas.stringToDate(jsonCompra.getString("fechaCompraCompletada")));
        
        if(pagada)
            compra.setFechaPago(Fechas.stringToDate(jsonCompra.getString("fechaPago")));
                
        JsonArray lineasCompra = jsonCompra.getJsonArray("lineasCompra");
        
        for(int i=0 ; i<lineasCompra.size() ; i++ )
            compra.reconstruirLineaCompra(lineasCompra.getJsonObject(i));
        
        return compra;
    }
    
    private void reconstruirLineaCompra(JsonObject linea)
            throws SQLException {
        lineasCompra.add(LineaCompra.getLineaCompra(linea));   
    }
}
