package modelo.controladores;

import datos.GestorPersistenciaPersona;
import java.sql.SQLException;
import modelo.Empleado;
import utilidades.excepciones.LoginException;

/**
 * 
 * @author grupo 03 - Diseno de Software
 */
public class ControladorCUIdentificarse {
    
    private Empleado empleado;
    
    public void getEmpleado(String login, String password)
            throws SQLException, LoginException {
        if(GestorPersistenciaPersona.realizarLogin(login, password))
            empleado = Empleado.getEmpleado(login);
    }

    public char obtenerTipoEmpleado() {
        return empleado.getTipoEmpleado();
    }
    
}
