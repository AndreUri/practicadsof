package modelo.controladores;

import java.sql.SQLException;
import java.util.List;
import modelo.Compra;
import modelo.LineaCompra;
import utilidades.excepciones.CompraException;
import utilidades.excepciones.LineasNoRecibidasException;

/**
 * 
 * @author grupo 03 - Diseno de Software
 */
public class ControladorCURegistrarCompra {
    
    private Compra compra;
    
    public List<LineaCompra> getLineasNoRecibidas(){
        return compra.getLineasCompraNoRecibidas();
    }
    
    public String getBodega() throws SQLException {
        return compra.getBodega().getNombre();
    }
    
    public LineaCompra getLineaDesdeReferencia(int referencia){
        for(LineaCompra l : getLineasNoRecibidas()){
            if(l.getReferencia() == referencia)
                return l;
        }
        return null;
    }
    
    public void getCompraRecibida(int idCompra) 
            throws CompraException, SQLException {
        if(!Compra.completada(idCompra))
            compra = Compra.getCompra(idCompra);
    }
    
    public void recibirLinea(int idLinea) {
        compra.recibirLinea(idLinea);
    }
    
    public void finalizarSeleccion() 
            throws LineasNoRecibidasException, SQLException {
        compra.completarEnPersistencia();
        List<LineaCompra> lineasNoRecibidas = compra.getLineasCompraNoRecibidas();
        if(!lineasNoRecibidas.isEmpty())
            throw new LineasNoRecibidasException(lineasNoRecibidas);
    }
    
    public void cancelar(){
        compra = null;
    }
    
}
