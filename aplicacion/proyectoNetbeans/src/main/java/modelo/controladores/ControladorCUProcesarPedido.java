package modelo.controladores;

import java.sql.SQLException;
import java.util.Date;
import modelo.Abonado;
import modelo.Pedido;
import modelo.Referencia;
import utilidades.Fechas;
import utilidades.excepciones.AbonadoException;
import utilidades.excepciones.AbonadoConPlazosPendientesException;
import utilidades.excepciones.ReferenciaException;

/**
 * 
 * @author grupo 03 - Diseno de Software
 */
public class ControladorCUProcesarPedido {
    
    private Abonado abonado;
    private Pedido pedido;
    
    public Abonado getAbonado(){
        return abonado;
    }

    public Pedido getPedido(){
        return pedido;
    }
    
    public void introducirAbonado(int numeroAbonado) 
            throws AbonadoException, SQLException {
        if(Abonado.existeIdAbonado(numeroAbonado))
            abonado = Abonado.getAbonado(numeroAbonado);
    }
    
    public void eliminarAbonado() {
        abonado = null;
    }
    
    public void crearPedido() 
            throws AbonadoConPlazosPendientesException, SQLException {
        Date fechaActual = Fechas.getCurrentDate();
        pedido = abonado.crearPedido(fechaActual);
    }
    
    public void addLinea(int referencia, int cantidad)
            throws ReferenciaException, SQLException {
        if(Referencia.isValida(referencia))
            pedido.addLineaPedido(cantidad,referencia);
    }
    
    public void finalizarPedido()
            throws SQLException {
        pedido.guardarPedido();
    }
    
    public void cancelar(){
        abonado = null;
        pedido = null;
    }
    
}
