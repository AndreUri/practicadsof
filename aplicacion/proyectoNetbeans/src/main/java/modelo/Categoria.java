package modelo;

/**
 * 
 * @author grupo 03 - Diseno de Software
 */
public enum Categoria {
    DELANO,
    CRIANZA,
    RESERVA,
    GRANRESERVA;
}
