package modelo;

/**
 * 
 * @author grupo 03 - Diseno de Software
 */
public enum EstadoPedido {
    PENDIENTE,
    TRAMITADO,
    COMPLETADO,
    SERVIDO,
    FACTURADO,
    PAGADO;
    
    public static EstadoPedido getEstadoAsociado(String estado){
        switch(estado.charAt(0)){
            case 'P':
                return PENDIENTE;
            case 'T':
                return TRAMITADO;
            case 'C':
                return COMPLETADO;
            case 'S':
                return SERVIDO;
            case 'F':
                return FACTURADO;
            case 'A':
                return PAGADO;
            default:
                return null;
        }
    }
}
