package modelo;

import datos.GestorPersistenciaPersona;
import java.sql.SQLException;
import java.util.Date;
import javax.json.JsonObject;
import utilidades.excepciones.AbonadoConPlazosPendientesException;
import utilidades.excepciones.AbonadoIdInexistenteException;

/**
 * 
 * @author grupo 03 - Diseno de Software
 */
public class Abonado extends Persona{
    
    private final int     numeroAbonado;
    private final String  openIDref; 
    
    public Abonado(int numeroAbonado, String openIDref, String nif, String nombre, String apellidos, String direccion, String telefono, String email, String cuentaBancaria) {
        super(nif, nombre, apellidos, direccion, telefono, email, cuentaBancaria);
        this.numeroAbonado = numeroAbonado;
        this.openIDref = openIDref;
    }
    
    public int getNumeroAbonado() {
        return numeroAbonado;
    }

    public String getOpenIDref() {
        return openIDref;
    }
    
    public boolean tienePlazosDePagoVencidos()
            throws SQLException {
        return GestorPersistenciaPersona.plazosDePagoVencidos(numeroAbonado);
    }

    public Pedido crearPedido(Date fechaCreacion) 
            throws SQLException, AbonadoConPlazosPendientesException{
        if(tienePlazosDePagoVencidos()){
            throw new AbonadoConPlazosPendientesException("El Abonado " 
                    + getNombre() + " " + getApellidos() + " tiene plazos "
                    + "de pago pendientes, notificarle en la dirección: "
                    + getEmail());
        }
        return new Pedido(numeroAbonado,fechaCreacion);
    }
    
    public static boolean existeIdAbonado(int idAbonado)
            throws SQLException, AbonadoIdInexistenteException {
        return GestorPersistenciaPersona.existNumeroAbonado(idAbonado);
    }
    
    public static Abonado getAbonado(int numeroAbonado) 
            throws SQLException  {
        
        // TODO: evitar que pueda pasarse un numeroAbonado incorrecto, que el metodo tiene visibilidad publica
        
        JsonObject abonado = GestorPersistenciaPersona.getDatosAbonado(numeroAbonado);
        
        // TODO: aplicar algún patrón creacional y extraer de aquí (?)
        String nif            = abonado.getString("nif");
        String nombre         = abonado.getString("nombre");
        String apellidos      = abonado.getString("apellidos");
        String direccion      = abonado.getString("direccion");
        String telefono       = abonado.getString("telefono");
        String email          = abonado.getString("email");
        String cuentaBancaria = abonado.getString("cuentaBancaria");
        
        int    numAb          = abonado.getInt("numeroAbonado");
        String openIDRef      = abonado.getString("openIDref");
        
        return new Abonado(numAb,openIDRef,nif,nombre,apellidos,direccion,telefono,email,cuentaBancaria);
    }
    
}
