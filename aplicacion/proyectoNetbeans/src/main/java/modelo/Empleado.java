package modelo;

import datos.GestorPersistenciaPersona;
import java.sql.SQLException;
import java.util.Date;
import javax.json.JsonObject;
import utilidades.Fechas;
import utilidades.excepciones.LoginException;

/**
 * 
 * @author grupo 03 - Diseno de Software
 */
public class Empleado extends Persona{

    private final String login;
    private final Date fechaInicio;
    private final char tipoEmpleado;

    // TODO: patrón creacional del estilo de builder
    public Empleado(String nif, String nombre, String apellidos, String direccion, String telefono, String email, String cuentaBancaria, String login, Date fechaInicio, char tipoEmpleado) {
        super(nif,nombre,apellidos,direccion,telefono,email,cuentaBancaria);
        this.login = login;
        this.fechaInicio = fechaInicio;
        this.tipoEmpleado = tipoEmpleado;
    }
    
    public String getLogin() {
        return login;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public char getTipoEmpleado() {
        return tipoEmpleado;
    }
    
    public static Empleado getEmpleado(String login) 
            throws SQLException, LoginException  {
        
        JsonObject empleado = GestorPersistenciaPersona.getPerfilUsuario(login);
 
        // TODO: aplicar algún patrón creacional
        String nif            = empleado.getString("nif");
        String nombre         = empleado.getString("nombre");
        String apellidos      = empleado.getString("apellidos");
        String direccion      = empleado.getString("direccion");
        String telefono       = empleado.getString("telefono");
        String email          = empleado.getString("email");
        String cuentaBancaria = empleado.getString("cuentaBancaria");
         
        String log            = empleado.getString("login");
        Date fechaIni         = Fechas.stringToDate(empleado.getString("fechaInicio"));
        char tipo             = empleado.getString("tipoEmpleado").charAt(0);
        
        return new Empleado(nif,nombre,apellidos,direccion,telefono,email,cuentaBancaria,log,fechaIni,tipo);
    }
}
