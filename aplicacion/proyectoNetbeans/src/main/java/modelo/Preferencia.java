package modelo;

/**
 * 
 * @author grupo 03 - Diseno de Software
 */
public class Preferencia {
    private DenominacionOrigen denominacion;
    private Categoria categoria;

    public Preferencia(DenominacionOrigen denominacion, Categoria categoria) {
        this.denominacion = denominacion;
        this.categoria = categoria;
    }

    /**
     * @return the denominacion
     */
    public DenominacionOrigen getDenominacion() {
        return denominacion;
    }

    /**
     * @param denominacion the denominacion to set
     */
    public void setDenominacion(DenominacionOrigen denominacion) {
        this.denominacion = denominacion;
    }

    /**
     * @return the categoria
     */
    public Categoria getCategoria() {
        return categoria;
    }

    /**
     * @param categoria the categoria to set
     */
    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }
    
}
