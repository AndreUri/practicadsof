package modelo;

/**
 * 
 * @author grupo 03 - Diseno de Software
 */
public enum EstadoFactura {
    EMITIDA,
    VENCIDA,
    PAGADA;
    
    public static EstadoFactura getEstadoAsociado(String estado){
        switch(estado.charAt(0)){
            case 'E':
                return EMITIDA;
            case 'V':
                return VENCIDA;
            case 'P':
                return PAGADA;
            default:
                return null;
        }
    }
}