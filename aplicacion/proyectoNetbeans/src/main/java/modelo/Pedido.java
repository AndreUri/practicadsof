package modelo;

import datos.GestorPersistenciaBodega;
import datos.GestorPersistenciaPedido;
import java.util.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.json.JsonArray;
import javax.json.JsonObject;
import utilidades.Fechas;
import utilidades.excepciones.AbonadoIdInexistenteException;

/**
 * 
 * @author grupo 03 - Diseno de Software
 */
public class Pedido {
    
    private int numero;
    private EstadoPedido estado;
    private final Date fechaRealizacion; 
    private String notaEntrega;
    private double importe;
    private Date fechaRecepcion = null;
    private Date fechaEntrega = null;
    private final List<LineaPedido> lineasPedido = new ArrayList<>();
    private int numeroAbonado;

    protected Pedido(int numeroAbonado, Date fechaRealizacion) {
        this.numeroAbonado = numeroAbonado;
        estado = EstadoPedido.PENDIENTE;
        this.fechaRealizacion = fechaRealizacion;
        this.importe = 0.0;
    }

    private Pedido(int numero, EstadoPedido estado, Date fechaRealizacion, String notaEntrega, double importe, Date fechaRecepcion, Date fechaEntrega, int numeroAbonado) {
        this(numeroAbonado,fechaRealizacion);
        this.numero = numero;
        this.estado = estado;
        this.notaEntrega = notaEntrega;
        this.importe = importe;
        this.fechaRecepcion = fechaRecepcion;
        this.fechaEntrega = fechaEntrega;
    }
    
    public int getNumero() {
        return numero;
    }
    
    private void setNumero(int numero) {
        this.numero = numero;
    }

    public EstadoPedido getEstado() {
        return estado;
    }

    private void setEstado(EstadoPedido estado) {
        this.estado = estado;
    }

    public Date getFechaRealizacion() {
        return fechaRealizacion;
    }
    
    public String getNotaEntrega() {
        return notaEntrega;
    }

    public double getImporte() {
        return importe;
    }

    public Date getFechaRecepcion() {
        return fechaRecepcion;
    }

    private void setFechaRecepcion(Date fechaRecepcion) {
        this.fechaRecepcion = fechaRecepcion;
    }

    public Date getFechaEntrega() {
        return fechaEntrega;
    }

    public void setFechaEntrega(Date fechaEntrega) {
        this.fechaEntrega = fechaEntrega;
    }
    
    public Abonado getAbonado()
            throws SQLException {
        return Abonado.getAbonado(numeroAbonado);
    }
    
    public void addLineaPedido(int unidades, int codigoReferencia) 
            throws SQLException {
        LineaPedido nuevaLinea = new LineaPedido(unidades,codigoReferencia);
        lineasPedido.add(nuevaLinea);
        double precio = GestorPersistenciaBodega.getPrecioReferencia(codigoReferencia);
        importe += precio;
    }
    
    private void reconstruirLineaPedido(JsonObject linea)
            throws SQLException {
        lineasPedido.add(LineaPedido.getLineaPedido(linea));   
    }
    
    public boolean todasLasLineasRecibidas(){
        for(LineaPedido l : lineasPedido){
            if(!l.isCompletada())
                return false;
        }
        return true;
    }
    
    public void completarEnPersistencia()
            throws SQLException {
        if(estado.equals(EstadoPedido.COMPLETADO))
            GestorPersistenciaPedido.cambiarEstadoPedido(numero, 'C');
    }
    
    public void guardarPedido()
            throws SQLException {
        
        java.sql.Date fechaCreacion = Fechas.getSQLDate(fechaRealizacion);
        
        Abonado abonado = Abonado.getAbonado(numeroAbonado);
        
        int idPedido = GestorPersistenciaPedido.crearPedido(fechaCreacion,
                notaEntrega, importe, abonado.getNif(), abonado.getNumeroAbonado());
        setNumero(idPedido);
        
        // Demasiadas aperturas/cierres de conexion ??
        for(LineaPedido lp : lineasPedido)
            LineaPedido.guardarLineaPedido(numero, lp);
    }
    
    public static Pedido getPedido(int numero)
            throws SQLException {
        
        // TODO: Se comprueba que esté en repositorio
        
        JsonObject jsonPedido = GestorPersistenciaPedido.getPedido(numero);
        
        int numeroAbonado     = jsonPedido.getInt("numeroAbonado");
        Date fechaRealizacion = Fechas.stringToDate(jsonPedido.getString("fechaRealizacion"));
        EstadoPedido estado   = EstadoPedido.getEstadoAsociado(jsonPedido.getString("estado"));
        String notaEntrega    = jsonPedido.getString("notaEntrega");
        double importe        = jsonPedido.getJsonNumber("importe").doubleValue();
        Date fechaRecepcion   = jsonPedido.isNull("fechaRecepcion") ? null :
                Fechas.stringToDate(jsonPedido.getString("fechaRecepcion"));
        Date fechaEntrega     = jsonPedido.isNull("fechaEntrega") ? null :
                Fechas.stringToDate(jsonPedido.getString("fechaEntrega"));
        
        Pedido pedido = new Pedido(numero,estado,fechaRealizacion,notaEntrega,importe,fechaRecepcion,fechaEntrega,numeroAbonado);
                
        JsonArray lineasPedido = jsonPedido.getJsonArray("lineasPedido");
        
        for(int i=0 ; i<lineasPedido.size() ; i++ )
            pedido.reconstruirLineaPedido(lineasPedido.getJsonObject(i));
        
        return pedido;
    }
    
    public static List<Pedido> getListaPedidos(List<Integer> idsP)
            throws SQLException {
        List<Pedido> pedidos = new ArrayList<>();
        
        for(Integer idP : idsP)
            pedidos.add(getPedido(idP));
  
        return pedidos.isEmpty() ? null : pedidos;
    }
    
    public static List<Pedido> getPedidosTramitados()
            throws SQLException {
        return getListaPedidos(GestorPersistenciaPedido.getIdsPedidosTramitados());
    }

}
