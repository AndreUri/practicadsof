package modelo;

<<<<<<< HEAD:aplicacion/proyectoNetbeans/src/vinoteca/modelo/clasesDominio/Bodega.java
=======
import datos.GestorPersistenciaBodega;
import java.sql.SQLException;
import javax.json.JsonObject;

>>>>>>> c8b25a8039b68dfd4a5a3f7c41357314e5355d60:aplicacion/proyectoNetbeans/src/main/java/modelo/Bodega.java
/**
 * 
 * @author grupo 03 - Diseno de Software
 */
public class Bodega {
    
    private String nombre;
    private String cif;
    private String direccion;

    public Bodega(String nombre, String cif, String direccion) {
        this.nombre = nombre;
        this.cif = cif;
        this.direccion = direccion;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the cif
     */
    public String getCif() {
        return cif;
    }

    /**
     * @param cif the cif to set
     */
    public void setCif(String cif) {
        this.cif = cif;
    }

    /**
     * @return the direccion
     */
    public String getDireccion() {
        return direccion;
    }

    /**
     * @param direccion the direccion to set
     */
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
    
    public static Bodega getBodega(int idVino) 
            throws SQLException  {
        
        JsonObject bodega = GestorPersistenciaBodega.getBodega(idVino);
        
        String nombre         = bodega.getString("nombre");
        String cif            = bodega.getString("cif");
        String direccion      = bodega.getString("direccion");
        
        return new Bodega(nombre,cif,direccion);
    }
    
}
