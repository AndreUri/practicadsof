package modelo;

/**
 * 
 * @author grupo 03 - Diseno de Software
 */
public class Persona {
    
    private String nif;
    private String nombre;
    private String apellidos;
    private String direccion;
    private String telefono;
    private String email;
    private String cuentaBancaria; //String[0..1]

    public Persona(String nif, String nombre, String apellidos, String direccion, String telefono, String email, String cuentaBancaria) {
        this.nif = nif;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.direccion = direccion;
        this.telefono = telefono;
        this.email = email;
        this.cuentaBancaria = cuentaBancaria;
    }

    public String getNif() {
        return nif;
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public String getDireccion() {
        return direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public String getEmail() {
        return email;
    }

    public String getCuentaBancaria() {
        return cuentaBancaria;
    }

}
