package modelo;

import datos.GestorPersistenciaCompra;
import datos.GestorPersistenciaPedido;
import java.util.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.json.JsonArray;
import javax.json.JsonObject;
import utilidades.Fechas;

/**
 * 
 * @author grupo 03 - Diseno de Software
 */
public class LineaCompra {
    
    private final int idLinea;
    private int unidades;
    private boolean recibida;
    private Date fechaRecepcion;
    private List<LineaPedido> lineasPedido = new ArrayList<>();

    public LineaCompra(int idLinea, int unidades, boolean recibida, Date fechaRecepcion) {
        this.idLinea = idLinea;
        this.unidades = unidades;
        this.recibida = recibida;
        this.fechaRecepcion = fechaRecepcion;
    }
    
    public int getId() {
        return idLinea;
    }
    
    public int getUnidades() {
        return unidades;
    }

    public boolean isRecibida() {
        return recibida;
    }
    
    public Date getFechaRecepcion() {
        return fechaRecepcion;
    }
    
    public void addLineaPedido(LineaPedido l){
        lineasPedido.add(l);
    }

    public List<LineaPedido> getLineasPedido() {
        return lineasPedido;
    }

    public int getReferencia(){
        return lineasPedido.get(0).getReferencia();
    }
    
// METODOS PARA EL CAMBIO DE ESTADO:
    
    public void recibir(){
        if(!recibida){
            setRecibida(true);
            setFechaRecepcion(Fechas.getCurrentDate());
            
            for(LineaPedido l : lineasPedido)
                l.completar();
        }
    }
    
    public void resetear(){
        if(recibida){
            setRecibida(false);
            setFechaRecepcion(null);
            
            for(LineaPedido l : lineasPedido)
                l.resetear();
        }
    }

    private void setRecibida(boolean recibida) {
        this.recibida = recibida;
    }

    private void setFechaRecepcion(Date fechaRecepcion) {
        this.fechaRecepcion = fechaRecepcion;
    }
    
    protected void completarEnPersistencia()
            throws SQLException {
        if(recibida){
            // Cuidado con sobreescribir la fecha de recepcion:
            GestorPersistenciaCompra.completarLineaCompra(idLinea, Fechas.getSQLDate(fechaRecepcion));

            for(LineaPedido l : lineasPedido)
                l.completarEnPersistencia();
        }
    }
    
    // METODOS PARA LA RECUPERACIÓN DE BDD
    
    public static LineaCompra getLineaCompra(JsonObject lineaCompra)
            throws SQLException {
        
        int idLinea         = lineaCompra.getInt("id");
        int unidades        = lineaCompra.getInt("unidades");
        boolean recibida    = lineaCompra.getBoolean("recibida");
        Date fechaRecepcion = lineaCompra.isNull("fechaRecepcion") ? null : 
                Fechas.stringToDate(lineaCompra.getString("fechaRecepcion"));
        
        JsonArray lineas = GestorPersistenciaPedido.getLineasPedidoAsociadas(idLinea);
        
        LineaCompra lc = new LineaCompra(idLinea,unidades,recibida,fechaRecepcion);
        
        for(int i=0 ; i<lineas.size() ; i++ )
            lc.addLineaPedido(LineaPedido.getLineaPedido(lineas.getJsonObject(i)));
        
        return lc;
    }
    
}
