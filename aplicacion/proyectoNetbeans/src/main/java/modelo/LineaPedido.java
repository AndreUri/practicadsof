package modelo;

import datos.GestorPersistenciaPedido;
import java.sql.SQLException;
import javax.json.JsonObject;

/**
 * 
 * @author grupo 03 - Diseno de Software
 */
public class LineaPedido {
    
    private int id;
    private final int unidades;
    private boolean completada;
    private final int referencia;
    private int idLineaCompra;
    
    protected LineaPedido(int unidades, int referencia) {
        this.unidades = unidades;
        completada = false;
        this.referencia = referencia;
    }

    private LineaPedido(int id, int unidades, boolean completada, int referencia) {
        this.unidades = unidades;
        this.completada = completada;
        this.referencia = referencia;
    }
    
    private void setId(int id){
        this.id = id;
    }
    
    public int getUnidades() {
        return unidades;
    }

    public boolean isCompletada() {
        return completada;
    }

    private void setCompletada(boolean completada) {
        this.completada = completada;
    }

    public int getReferencia() {
        return referencia;
    }

    private void setLineaCompra(int lineaCompra) {
        this.idLineaCompra = lineaCompra;
    }

    public int getLineaCompra() {
        return idLineaCompra;
    }
    
    public void completar(){
        if(!completada)
            setCompletada(true);
    }
    
    public void resetear(){
        if(completada)
            setCompletada(false);
        
    }
    
    protected void completarEnPersistencia()
            throws SQLException {
        if(completada)
            GestorPersistenciaPedido.completarLineasPedido(id);
    }
    
    protected static void guardarLineaPedido(int numeroPedido, LineaPedido linea)
            throws SQLException {
        int id = GestorPersistenciaPedido.crearLineaPedido(numeroPedido, linea.unidades, linea.referencia);
        linea.setId(id);
    }
    
    public static LineaPedido getLineaPedido(JsonObject lineaPedido){
        
        int id             = lineaPedido.getInt("id");
        int unidades       = lineaPedido.getInt("unidades");
        boolean completada = lineaPedido.getBoolean("completada");
        int codigo         = lineaPedido.getInt("codigo");
        
        LineaPedido linea = new LineaPedido(id,unidades,completada,codigo);
        
        if(!lineaPedido.isNull("idLineaCompra"))
            linea.setLineaCompra(lineaPedido.getInt("idLineaCompra"));
        
        return linea;
    }
    
}
