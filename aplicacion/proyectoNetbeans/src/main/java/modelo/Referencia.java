package modelo;

import datos.GestorPersistenciaBodega;
import java.sql.SQLException;
import javax.json.JsonObject;
import utilidades.excepciones.ReferenciaException;

/**
 * 
 * @author grupo 03 - Diseno de Software
 */
public class Referencia {
    
    private final int codigo;
    private final boolean esPorCajas;
    private final int contenidoCL;
    private final double precio;
    private final boolean disponible;
    private final int vino;

    public Referencia(int codigo, boolean esPorCajas, int contenidoCL, double precio, boolean disponible,int vino) {
        this.codigo = codigo;
        this.esPorCajas = esPorCajas;
        this.contenidoCL = contenidoCL;
        this.precio = precio;
        this.disponible = disponible;
        this.vino = vino;
    }

    public int getCodigo() {
        return codigo;
    }

    public boolean isEsPorCajas() {
        return esPorCajas;
    }

    public int getContenidoCL() {
        return contenidoCL;
    }

    public double getPrecio() {
        return precio;
    }

    public boolean isDisponible() {
        return disponible;
    }

    public Bodega getBodega() 
            throws SQLException {
        return Bodega.getBodega(vino);
    }
    
    public static boolean isValida(int codigo)
            throws SQLException, ReferenciaException {
        return GestorPersistenciaBodega.comprobarReferencia(codigo);
    }
    
    public static Referencia getReferencia(int codigo)
            throws SQLException {
        
        JsonObject referencia = GestorPersistenciaBodega.getReferencia(codigo);
        
        boolean esPorCajas    = referencia.getBoolean("esPorCajas");
        int contenidoCL       = referencia.getInt("contenidoEnCL");
        double precio         = referencia.getJsonNumber("precio").doubleValue();
        boolean disponible    = referencia.getBoolean("disponible");
        int vino              = Vino.getVino(codigo);
        
        return new Referencia(codigo,esPorCajas,contenidoCL,precio,disponible,vino);
    }
    
}
