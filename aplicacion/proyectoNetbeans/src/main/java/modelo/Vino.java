package modelo;

import datos.GestorPersistenciaBodega;
import java.sql.SQLException;

/**
 * 
 * @author grupo 03 - Diseno de Software
 */
public class Vino {
    
    private final String nombreComercial;
    private final int ano;
    private final DenominacionOrigen denominacion;
    private final Categoria categoria;
    private final String comentario;
    private final Bodega bodega;

    public Vino(String nombreComercial, int ano, DenominacionOrigen denominacion, Categoria categoria, String comentario,Bodega bodega) {
        this.nombreComercial = nombreComercial;
        this.ano = ano;
        this.denominacion = denominacion;
        this.categoria = categoria;
        this.comentario = comentario;
        this.bodega = bodega;
    }
    
    
    public String getNombreComercial() {
        return nombreComercial;
    }

    public int getAno() {
        return ano;
    }

    public DenominacionOrigen getDenominacion() {
        return denominacion;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public String getComentario() {
        return comentario;
    }
    
    public Bodega getBodega(){
        return bodega;
    }
    
    public static int getVino(int idReferencia)
            throws SQLException {
        return GestorPersistenciaBodega.getIdVino(idReferencia);
    }
    
}
