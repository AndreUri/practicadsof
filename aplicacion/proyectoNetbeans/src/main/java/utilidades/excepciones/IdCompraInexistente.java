package utilidades.excepciones;

/**
 * 
 * @author grupo 03 - Diseno de Software
 */
public class IdCompraInexistente extends CompraException {
    
    public IdCompraInexistente() { 
        super(); 
    }

    public IdCompraInexistente(String message) {
        super(message); 
    }
    
}