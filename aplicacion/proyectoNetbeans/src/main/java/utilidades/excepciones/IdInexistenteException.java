package utilidades.excepciones;

/**
 * 
 * @author grupo 03 - Diseno de Software
 */
public class IdInexistenteException extends LoginException {
    
    public IdInexistenteException() { 
        super(); 
    }

    public IdInexistenteException(String message) {
        super(message); 
    }
}