package utilidades.excepciones;

/**
 * 
 * @author grupo 03 - Diseno de Software
 */
public class CompraException extends Exception {
    
    public CompraException() { 
        super(); 
    }
    
    public CompraException(String message) {
        super(message); 
    }

}