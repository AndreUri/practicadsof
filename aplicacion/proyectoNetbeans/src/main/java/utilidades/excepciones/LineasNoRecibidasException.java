package utilidades.excepciones;

import java.util.List;

/**
 * 
 * @author grupo 03 - Diseno de Software
 */
public class LineasNoRecibidasException extends CompraException {
    
    public List<?> lineasNoRecibidas;
    
    public LineasNoRecibidasException() { 
        super(); 
    }

    public LineasNoRecibidasException(String message) {
        super(message); 
    }
    
    public LineasNoRecibidasException(List<?> lineasNoRecibidas) {
        this.lineasNoRecibidas = lineasNoRecibidas;
    }
    
}