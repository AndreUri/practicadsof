package utilidades.excepciones;

/**
 * 
 * @author grupo 03 - Diseno de Software
 */
public class ReferenciaException extends Exception {
    
    public ReferenciaException() { 
        super(); 
    }
    
    public ReferenciaException(String message) {
        super(message); 
    }

}
