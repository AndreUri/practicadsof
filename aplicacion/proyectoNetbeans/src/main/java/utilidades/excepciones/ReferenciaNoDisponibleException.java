package utilidades.excepciones;

/**
 * 
 * @author grupo 03 - Diseno de Software
 */
public class ReferenciaNoDisponibleException extends ReferenciaException {
    
    public ReferenciaNoDisponibleException() { 
        super(); 
    }

    public ReferenciaNoDisponibleException(String message) {
        super(message); 
    }
    
}