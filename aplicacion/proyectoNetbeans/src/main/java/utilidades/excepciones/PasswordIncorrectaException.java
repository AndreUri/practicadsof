package utilidades.excepciones;

/**
 * 
 * @author grupo 03 - Diseno de Software
 */
public class PasswordIncorrectaException extends LoginException {
    
    public PasswordIncorrectaException() { 
        super(); 
    }

    public PasswordIncorrectaException(String message) {
        super(message); 
    }
}