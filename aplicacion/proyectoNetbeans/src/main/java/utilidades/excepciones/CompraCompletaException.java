package utilidades.excepciones;

/**
 * 
 * @author grupo 03 - Diseno de Software
 */
public class CompraCompletaException extends CompraException {
    
    public CompraCompletaException() { 
        super(); 
    }

    public CompraCompletaException(String message) {
        super(message); 
    }
    
}