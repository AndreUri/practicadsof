package utilidades.excepciones;

/**
 * 
 * @author grupo 03 - Diseno de Software
 */
public class AbonadoIdInexistenteException extends AbonadoException {
    
    public AbonadoIdInexistenteException() { 
        super(); 
    }

    public AbonadoIdInexistenteException(String message) {
        super(message); 
    }
    
}