package utilidades.excepciones;

/**
 * 
 * @author grupo 03 - Diseno de Software
 */
public class LoginException extends Exception {
    
    public LoginException() { 
        super(); 
    }
    
    public LoginException(String message) {
        super(message); 
    }

}
