package utilidades.excepciones;

/**
 * 
 * @author grupo 03 - Diseno de Software
 */
public class AbonadoConPlazosPendientesException extends AbonadoException {
    
    public AbonadoConPlazosPendientesException() { 
        super(); 
    }

    public AbonadoConPlazosPendientesException(String message) {
        super(message); 
    }
    
}