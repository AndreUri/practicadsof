package utilidades.excepciones;

/**
 * 
 * @author grupo 03 - Diseno de Software
 */
public class AbonadoException extends Exception {
    
    public AbonadoException() { 
        super(); 
    }
    
    public AbonadoException(String message) {
        super(message); 
    }
}
