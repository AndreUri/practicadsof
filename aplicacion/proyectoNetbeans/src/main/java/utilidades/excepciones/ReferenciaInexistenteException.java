package utilidades.excepciones;

/**
 * 
 * @author grupo 03 - Diseno de Software
 */
public class ReferenciaInexistenteException extends ReferenciaException {
    
    public ReferenciaInexistenteException() { 
        super(); 
    }

    public ReferenciaInexistenteException(String message) {
        super(message); 
    }
    
}