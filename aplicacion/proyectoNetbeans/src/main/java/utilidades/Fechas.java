package utilidades;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Calendar;

/**
 * 
 * @author grupo 03 - Diseno de Software
 */
public class Fechas {
    
    public static Date getCurrentDate() {
        return Calendar.getInstance().getTime();
    }
    
    public static java.sql.Date getCurrentSQLDate(){
        return new java.sql.Date(Calendar.getInstance().getTimeInMillis());
    }
    
    public static java.sql.Date getSQLDate(Date fecha){
        return new java.sql.Date(fecha.getTime());
    }
    
    public static Date stringToDate(String fecha){
        SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
        try {
            return formato.parse(fecha);
        } catch (ParseException ex) {
            ex.printStackTrace();
        }
        return null;
    }
    
    public static boolean fechaAnteriorAlMesPasado(Date fecha){
        Calendar hoy = Calendar.getInstance();
        hoy.add(Calendar.DAY_OF_MONTH, -30);
        Date primerDiaMesPasado = hoy.getTime();
        return fecha.before(primerDiaMesPasado);
    }

}
