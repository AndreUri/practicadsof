package datos;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * 
 * @author grupo 03 - Diseno de Software
 */
public class ConexionBD implements IConexionBD {
    
    private static final String URLBD      = "jdbc:derby://localhost:1527/PracticaDSOF";
    private static final String DRIVERNAME = "org.apache.derby.jdbc.ClientDriver";
    private static final String USERNAME   = "usuario";
    private static final String PASSWORD   = "pass";
    
    private static ConexionBD conexionBD;
    private Connection conexion;

    private ConexionBD() throws SQLException, ClassNotFoundException{
    	Class.forName(DRIVERNAME);
    	conexion = DriverManager.getConnection(URLBD, USERNAME, PASSWORD);
    }
    
    public static ConexionBD getInstancia() throws SQLException, 
            ClassNotFoundException{
    	return conexionBD == null ? new ConexionBD() : conexionBD; 
    }
    
    public PreparedStatement prepareStatement(String query) throws SQLException{
        return conexion.prepareStatement(query,Statement.RETURN_GENERATED_KEYS);
    }

    public void cerrar() throws SQLException{
    	conexion.close();
    }

    @Override
    public ResultSet create(PreparedStatement query) throws SQLException {
        int affectedRows = query.executeUpdate();
        if( affectedRows > 0 )
            return query.getGeneratedKeys();
        return null;
    }

    @Override
    public ResultSet read(PreparedStatement query) throws SQLException {
        return query.executeQuery();
    }

    @Override
    public void update(PreparedStatement query) throws SQLException {
        query.executeUpdate();
    }

    @Override
    public void delete(PreparedStatement query) throws SQLException {
        query.executeUpdate();
    }


}
