package datos;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * 
 * @author grupo 03 - Diseno de Software
 */
public abstract class GestorPersistencia {
    
    protected static ConexionBD db;
    protected static PreparedStatement ps;
    protected static ResultSet rs;
    
    protected static void setUp(){
        try{
            db = ConexionBD.getInstancia();
        } catch(SQLException | ClassNotFoundException e) {
            // TODO:
            e.printStackTrace();
        }
    }
    
    protected static void tearDown(){
        try{
            ps.close();
            rs.close();
            // falta db.cerrar() ?
        } catch(SQLException e) {
            // TODO:
            e.printStackTrace();
        }
    }

}
