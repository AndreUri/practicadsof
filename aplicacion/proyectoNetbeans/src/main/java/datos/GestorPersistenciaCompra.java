package datos;

import java.sql.Date;
import java.sql.SQLException;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import utilidades.excepciones.CompraException;
import utilidades.excepciones.IdCompraInexistente;
import utilidades.excepciones.CompraCompletaException;

/**
 * 
 * @author grupo 03 - Diseno de Software
 */
public class GestorPersistenciaCompra extends GestorPersistencia {
    
    public static final String COMPRA       = "COMPRA";
    public static final String LINEACOMPRA  = "LINEACOMPRA";
    
    
    public static boolean comprobarCompraCompletada(int idCompra)
            throws SQLException, CompraException {
        
        setUp();
        ps = db.prepareStatement(
            "SELECT * FROM " + COMPRA + " WHERE idCompra = ? ");
        ps.setInt(1, idCompra);
        rs = db.read(ps);
        boolean existeCompra = rs.next();
        boolean compraCompleta = existeCompra ? rs.getString("recibidaCompleta").equals("T") : false ;
        tearDown();
        if(!existeCompra)
            throw new IdCompraInexistente("El Id de la compra no Existe");
        else if(compraCompleta)
            throw new CompraCompletaException("La compra consultada ya ha sido completada");
        return compraCompleta;
    }
    
    public static void completarCompra(int idCompra, Date fechaCompraCompletada)
            throws SQLException {
        setUp();
        ps = db.prepareStatement("UPDATE " + COMPRA + 
                " SET recibidaCompleta = 'T', fechaCompraCompletada = ? WHERE idCompra = ?");
        ps.setDate(1, fechaCompraCompletada);
        ps.setInt(2, idCompra);
        db.update(ps);
        tearDown();
    }
    
    public static void completarLineaCompra(int idLineaCompra, Date fechaRecepcion)
            throws SQLException {
        
        setUp();
        ps = db.prepareStatement("UPDATE " + LINEACOMPRA + 
                " SET recibida = 'T', fechaRecepcion = ? WHERE id = ?");
        ps.setDate(1, fechaRecepcion);
        ps.setInt(2, idLineaCompra);
        db.update(ps);
        tearDown();
    }
    
    public static JsonObject getCompra(int idCompra)
            throws SQLException {
        
        JsonObjectBuilder job = null;
        JsonObjectBuilder jobLinea;
        JsonArrayBuilder jab = Json.createArrayBuilder();
        
        setUp();
        ps = db.prepareStatement(
            "SELECT * FROM " + COMPRA + " C, " + LINEACOMPRA + " L " +
                    " WHERE C.idCompra = ? AND C.idCompra = L.idCompra");
        
        ps.setInt(1, idCompra);
        rs = db.read(ps);
        // Iteramos sobre todas las líneas de esa compra; en la primera iteración,
        // añadimos los datos generales de la compra
        while(rs.next()){
            
            // Si aún no se han añadido los datos de la compra, se extraen del resultado de la query
            if(job == null){
                
                job = Json.createObjectBuilder()
                    .add("idCompra",         rs.getInt("idCompra"))
                    .add("fechaInicioCompra",rs.getDate("fechaInicioCompra").toString())
                    .add("recibidaCompleta", rs.getString("recibidaCompleta").equals("T"))
                    .add("importe",          rs.getDouble("importe"))
                    .add("pagada",           rs.getString("pagada").equals("T"));
                
                Date fechaCompraCompletada = rs.getDate("fechaCompraCompletada");
                Date fechaPago             = rs.getDate("fechaPago");
                
                if(fechaCompraCompletada == null) 
                    job.addNull("fechaCompraCompletada");
                else 
                    job.add("fechaCompraCompletada", fechaCompraCompletada.toString());
                
                if(fechaPago == null) 
                    job.addNull("fechaPago");
                else 
                    job.add("fechaPago", fechaPago.toString());
            }
            
            //Y añadimos los datos particulares de la linea de compra:
            
            jobLinea = Json.createObjectBuilder()
                    .add("id",      rs.getInt("id"))
                    .add("unidades",rs.getInt("unidades"))
                    .add("recibida",rs.getString("recibida").charAt(0)=='T')
                    .add("idCompra",idCompra);
            
            Date fechaRecepcion = rs.getDate("fechaRecepcion");
            
            if(fechaRecepcion == null)
                jobLinea.addNull("fechaRecepcion");
            else
                jobLinea.add("fechaRecepcion", fechaRecepcion.toString());    
            
            jab.add(jobLinea.build());
        }
        
        job.add("lineasCompra", jab);
        tearDown();
        return job.build();
    }
    
}
