package datos;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import utilidades.excepciones.AbonadoIdInexistenteException;
import utilidades.excepciones.LoginException;
import utilidades.excepciones.IdInexistenteException;
import utilidades.excepciones.PasswordIncorrectaException;

/**
 * 
 * @author grupo 03 - Diseno de Software
 */
public class GestorPersistenciaPersona extends GestorPersistencia {
    
    public static final String PERSONA  = "PERSONA";
    public static final String EMPLEADO = "EMPLEADO";
    public static final String ABONADO  = "ABONADO";
    
    
    public static boolean realizarLogin(String login, String password) 
            throws SQLException, LoginException{
        
        setUp();
        ps = db.prepareStatement(
            "SELECT * FROM " + EMPLEADO + " WHERE login = ?" );
        ps.setString(1, login);
        rs = db.read(ps);
        boolean existeUsuario = rs.next();
        boolean passCorrecta  = existeUsuario ? rs.getString("password").equals(password) : false;
        tearDown();
        if(!existeUsuario)
            throw new IdInexistenteException("El login de empleado introducido no existe");
        else if(!passCorrecta)
            throw new PasswordIncorrectaException("El password introducido no es correcto");
        return existeUsuario;
    }
    
    private static JsonObjectBuilder getDatosPersonales(String nif)
            throws SQLException {
        
        JsonObjectBuilder datosPersonales = null;
        setUp();
        ps = db.prepareStatement(
            "SELECT * FROM " + PERSONA + " WHERE nif = ? " );
        
        ps.setString(1, nif);
        try (ResultSet aux = db.read(ps)) {
            if(aux.next()){
                datosPersonales = Json.createObjectBuilder()
                        .add("nif",             aux.getString("nif"))
                        .add("nombre",          aux.getString("nombre"))
                        .add("apellidos",       aux.getString("apellidos"))
                        .add("direccion",       aux.getString("direccion"))
                        .add("telefono",        aux.getString("telefono"))
                        .add("email",           aux.getString("email"));
                String cuentaBancaria = aux.getString("cuentaBancaria");
                
                if(cuentaBancaria!=null)
                    datosPersonales.add("cuentaBancaria", cuentaBancaria);
                else
                    datosPersonales.addNull("cuentaBancaria");                    
            }
        }
        ps.close();
        return datosPersonales;
    }
    
    public static JsonObject getPerfilUsuario(String login)
            throws SQLException{
        
        JsonObject resultado = null;
        
        setUp();
        ps = db.prepareStatement(
            "SELECT * FROM " + EMPLEADO +  " WHERE login = ? " );
        ps.setString(1, login);
        rs = db.read(ps);
        if(rs.next()){
            String nif = rs.getString("nif");
            resultado = getDatosPersonales(nif)
                    .add("login",           login)
                    .add("fechaInicio",     rs.getDate("fechaInicio").toString())
                    .add("tipoEmpleado",    rs.getString("tipoEmpleado"))
                    .build();
        }
        tearDown();
        return resultado;
    }
    
    public static boolean existNumeroAbonado(int numeroAbonado)
            throws SQLException, AbonadoIdInexistenteException{
        
        setUp();
        ps = db.prepareStatement(
            "SELECT * FROM " + ABONADO + " WHERE numeroabonado = ?" );
        ps.setInt(1, numeroAbonado);
        rs = db.read(ps);
        boolean existeNumeroAbonado = rs.next();
        tearDown();
        if(!existeNumeroAbonado)
            throw new AbonadoIdInexistenteException("No existe un abonado con id " + numeroAbonado);
        return existeNumeroAbonado;
    }
    
    public static JsonObject getDatosAbonado(int numeroAbonado)
            throws SQLException{
        
        JsonObject datosAbonado = null;
        
        setUp();
        ps = db.prepareStatement(
            "SELECT * FROM " + ABONADO + " WHERE numeroabonado = ? " );
        ps.setInt(1, numeroAbonado);
        rs = db.read(ps);
        if(rs.next()){
            String nif = rs.getString("nif");
            datosAbonado = getDatosPersonales(nif)
                .add("numeroAbonado",   numeroAbonado)
                .add("openIDref",       rs.getString("openIDref"))
                .build();
        }
        tearDown();
        return datosAbonado;
    }
    
    public static boolean plazosDePagoVencidos(int numeroAbonado)
            throws SQLException{
        boolean tienePlazosVencidos = GestorPersistenciaPedido.comprobarFacturasVencidas(numeroAbonado);
        return tienePlazosVencidos;
    }
    
}
