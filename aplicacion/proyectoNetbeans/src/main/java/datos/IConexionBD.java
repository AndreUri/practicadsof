package datos;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * 
 * @author grupo 03 - Diseno de Software
 */
public interface IConexionBD {
    
    public ResultSet create(PreparedStatement query) throws SQLException;
    
    public ResultSet read(PreparedStatement query) throws SQLException;
    
    public void update(PreparedStatement query) throws SQLException;
    
    public void delete(PreparedStatement query) throws SQLException;
    
}
