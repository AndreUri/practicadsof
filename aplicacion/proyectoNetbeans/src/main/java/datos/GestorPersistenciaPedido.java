package datos;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

/**
 * 
 * @author grupo 03 - Diseno de Software
 */
public class GestorPersistenciaPedido extends GestorPersistencia {
    
    public static final String PEDIDO       = "PEDIDO";
    public static final String LINEAPEDIDO  = "LINEAPEDIDO";
    public static final String FACTURA      = "FACTURA";
    
    public static boolean comprobarFacturasVencidas(int numeroAbonado)
            throws SQLException {
        
        setUp();
        ps = db.prepareStatement(
            "SELECT F.estado FROM " + PEDIDO + " P, " + FACTURA + " F " +
                    " WHERE P.numeroabonado = ? AND "
                    + "P.numerofactura = F.numerofactura AND "
                    + "F.estado = 'V' " );
        ps.setInt(1, numeroAbonado);
        rs = db.read(ps);
        boolean tieneFacturasVencidas = rs.next();
        tearDown();
        return tieneFacturasVencidas;
    }
    
    public static void completarLineasPedido(int idLinea)
            throws SQLException {
        
        setUp();
        ps = db.prepareStatement(
            "UPDATE " + LINEAPEDIDO + " SET completada = 'T' WHERE id = ?");
        ps.setInt(1, idLinea);
        db.update(ps);
        tearDown();
    }
    
    @Deprecated
    public static void completarLineasPedidoPorLineaCompra(int idLineaCompra)
            throws SQLException {
        
        setUp();
        ps = db.prepareStatement(
            "UPDATE " + LINEAPEDIDO + " SET completada = 'T' WHERE idLineaCompra = ?");
        ps.setInt(1, idLineaCompra);
        db.update(ps);
        tearDown();
    }
    
    public static void cambiarEstadoPedido(int numeroPedido, char nuevoEstado)
            throws SQLException {
        
        setUp();
        ps = db.prepareStatement(
            "UPDATE " + PEDIDO + " SET estado = ? WHERE id = ?");
        ps.setString(1, Character.toString(nuevoEstado));
        ps.setInt(2, numeroPedido);
        db.update(ps);
        tearDown();
    }
    
    public static int crearPedido(Date fecha, String notaEntrega, double importe, String nif, int numeroAbonado)
            throws SQLException {
        
        setUp();
        ps = db.prepareStatement(
            "INSERT INTO " + PEDIDO + " (estado,fechaRealizacion,notaEntrega,importe,nif,numeroAbonado) VALUES ('P',?,?,?,?,?)" );
        ps.setDate(1, fecha);
        ps.setString(2, notaEntrega);
        ps.setDouble(3, importe);
        ps.setString(4, nif);
        ps.setInt(5, numeroAbonado);
        rs = db.create(ps);
        
        int idPedido = -1;
        if(rs.next())
            idPedido = rs.getInt(1);
        
        tearDown();
        
        return idPedido;
    }
    
    /**
     * CREAR UNA LÍNEA DE PEDIDO NUEVA EN LA BASE DE DATOS
     * 
     * @param numeroPedido
     * @param unidades
     * @param codigoReferencia
     * @return
     * @throws SQLException 
     */
    public static int crearLineaPedido(int numeroPedido, int unidades, int codigoReferencia)
            throws SQLException {
        
        setUp();
        ps = db.prepareStatement(
            "INSERT INTO " + LINEAPEDIDO + " (unidades,codigo,numero) VALUES (?,?,?)" );
        
        ps.setInt(1, unidades);
        ps.setInt(2, codigoReferencia);
        ps.setInt(3, numeroPedido);
        rs = db.create(ps);
        int idLineaPedido = -1;
        if(rs.next())
            idLineaPedido = rs.getInt(1);
        tearDown();
        return idLineaPedido; 
    }
    
    public static JsonObject getPedido(int numeroPedido)
            throws SQLException {
        
        JsonObjectBuilder pedidoBuilder = null;
        JsonObjectBuilder jobLinea;
        JsonArrayBuilder jab = Json.createArrayBuilder();
        
        setUp();
        ps = db.prepareStatement(
            "SELECT * FROM " + PEDIDO + " P, " + LINEAPEDIDO + " L " +
                    " WHERE P.numero = ? AND P.numero = L.numero");
        
        ps.setInt(1, numeroPedido);
        rs = db.read(ps);
        while(rs.next()){
            
            if(pedidoBuilder == null){
                
                pedidoBuilder = Json.createObjectBuilder()
                    .add("numero",          numeroPedido)
                    .add("estado",          rs.getString("estado"))
                    .add("fechaRealizacion",rs.getDate("fechaRealizacion").toString())
                    .add("notaEntrega",     rs.getString("notaEntrega"))
                    .add("importe",         rs.getDouble("importe"))
                    .add("nif",             rs.getString("nif"))
                    .add("numeroAbonado",   rs.getInt("numeroAbonado"));
                
                Date fechaRecepcion = rs.getDate("fechaRecepcion");
                Date fechaEntrega   = rs.getDate("fechaEntrega");
                
                if(fechaRecepcion == null) 
                    pedidoBuilder.addNull("fechaRecepcion");
                else 
                    pedidoBuilder.add("fechaRecepcion", fechaRecepcion.toString());
                
                if(fechaEntrega == null) 
                    pedidoBuilder.addNull("fechaEntrega");
                else 
                    pedidoBuilder.add("fechaEntrega", fechaEntrega.toString());
                
                int numeroFactura = rs.getInt("numeroFactura");
                
                // Por como funciona el API de javax.json
                if(numeroFactura == 0)
                    pedidoBuilder.addNull("numeroFactura");
                else
                    pedidoBuilder.add("numeroFactura", numeroFactura);
            }
            jab.add(getLineaPedidoDesdeRs(rs));
        }
        pedidoBuilder.add("lineasPedido", jab);
        tearDown();
        return pedidoBuilder.build();
    }
    
    private static List<Integer> getIdsPedidos(String whereClause, Integer parametroDeBusqueda)
            throws SQLException {
        
        List<Integer> idsPedidos = new ArrayList<>();
        
        setUp();
        ps = db.prepareStatement(
            "SELECT numero FROM " + PEDIDO + whereClause);
        if(parametroDeBusqueda != null)
            ps.setInt(1, parametroDeBusqueda);
        rs = db.read(ps);
        while(rs.next())
            idsPedidos.add(rs.getInt(1));
        tearDown();
        
        return idsPedidos;
    }
    
    public static List<Integer> getIdsPedidosTramitados()
            throws SQLException {
        
        String whereClause = " WHERE estado = 'T' ";
        List<Integer> idsPedidos = getIdsPedidos(whereClause,null);
        return idsPedidos.isEmpty() ? null : idsPedidos;
    }
    
    public static List<Integer> getIdsPedidosAsociadosAFactura(int numeroFactura)
            throws SQLException {
        
        String whereClause = " WHERE numeroFactura = ? ";
        List<Integer> idsPedidos = getIdsPedidos(whereClause,numeroFactura);
        return idsPedidos.isEmpty() ? null : idsPedidos;
    }
    
    public static JsonArray getFacturasVencidasPreviasAFecha(Date fecha)
            throws SQLException {
        
        JsonArrayBuilder jab = Json.createArrayBuilder();
        
        setUp();
        ps = db.prepareStatement(
            "SELECT * FROM " + FACTURA + " WHERE estado = 'V' AND fechaEmision < ?");
        
        ps.setDate(1, fecha);
        rs = db.read(ps);
        
        while(rs.next())
            jab.add(getFacturaDesdeRs(rs));
        
        tearDown();
        JsonArray facturas = jab.build();
        return facturas;
    }
    
    private static JsonObject getFacturaDesdeRs(ResultSet facturaRs)
            throws SQLException {
        
        Date fechaPago = facturaRs.getDate("fechaPago");
        
        JsonObjectBuilder factura = Json.createObjectBuilder()
                .add("numeroFactura",       facturaRs.getInt("numeroFactura"))
                .add("fechaEmision",        facturaRs.getDate("fechaEmision").toString())
                .add("importe",             facturaRs.getDouble("importe"))
                .add("estado",              facturaRs.getString("estado"))
                .add("idExtractoBancario",  facturaRs.getString("idExtractoBancario"));
        
        if(fechaPago != null)
            factura.add("fechaPago",fechaPago.toString());
        else
            factura.addNull("fechaPago");
        
        return factura.build();
    }
    
    public static JsonArray getLineasPedidoAsociadas(int idLineaCompra)
            throws SQLException {
        
        JsonArrayBuilder jab = Json.createArrayBuilder();
        
        setUp();
        ps = db.prepareStatement(
            "SELECT * FROM " + LINEAPEDIDO + " WHERE idLineaCompra = ? ");
        
        ps.setInt(1, idLineaCompra);
        rs = db.read(ps);
        
        while(rs.next())
            jab.add(getLineaPedidoDesdeRs(rs));
        
        tearDown();
        JsonArray lineas = jab.build();
        return lineas;
    }
    
    private static JsonObject getLineaPedidoDesdeRs(ResultSet lineaRs)
            throws SQLException {
        
        JsonObjectBuilder linea = Json.createObjectBuilder()
                    .add("id",        lineaRs.getInt("id"))
                    .add("unidades",  lineaRs.getInt("unidades"))
                    .add("completada",lineaRs.getString("completada").equals("T"))
                    .add("codigo",    lineaRs.getInt("codigo"))
                    .add("numero",    lineaRs.getInt("numero"));
            
            int idLineaCompra = rs.getInt("idLineaCompra");
            
            if(idLineaCompra == 0)
                linea.addNull("idLineaCompra");
            else
                linea.add("idLineaCompra", idLineaCompra);
        
        return linea.build();
    }
    
}