package datos;

import java.sql.SQLException;
import javax.json.Json;
import javax.json.JsonObject;
import utilidades.excepciones.ReferenciaException;
import utilidades.excepciones.ReferenciaInexistenteException;
import utilidades.excepciones.ReferenciaNoDisponibleException;

/**
 * 
 * @author grupo 03 - Diseno de Software
 */
public class GestorPersistenciaBodega extends GestorPersistencia {
    
    public static final String BODEGA       = "BODEGA";
    public static final String VINO         = "VINO";
    public static final String REFERENCIA   = "REFERENCIA";
    public static final String DENOMINACION = "DENOMINACIONORIGEN";
    
    public static boolean comprobarReferencia(int codigo)
            throws SQLException, ReferenciaException{
        
        setUp();
        ps = db.prepareStatement(
            "SELECT * FROM " + REFERENCIA + " WHERE codigo = ?" );
        ps.setInt(1, codigo);
        rs = db.read(ps);
        boolean existeReferencia = rs.next();
        boolean referenciaDisponible = existeReferencia ? rs.getString("disponible").equals("T") : false;
        tearDown();
        if(!existeReferencia)
            throw new ReferenciaInexistenteException("La Referencia introducida no Existe");
        else if(!referenciaDisponible)
            throw new ReferenciaNoDisponibleException("La Referencia introducida no está disponible");
        return existeReferencia;
    }
      
    public static JsonObject getBodega(int idVino)
            throws SQLException {
        
        JsonObject bodega = null;
        
        setUp();
        ps = db.prepareStatement(
            "SELECT * FROM " + VINO + " V , " + BODEGA + " B WHERE V.id = ?");
        ps.setInt(1, idVino);
        rs = db.read(ps);
        if(rs.next()){
            bodega = Json.createObjectBuilder()
                    .add("nombre",   rs.getString("nombre"))
                    .add("cif",      rs.getString("cif"))
                    .add("direccion",rs.getString("direccion"))
                            .build();
        }
        tearDown();
        
        return bodega;
    }
    
    public static int getIdVino(int idReferencia)
            throws SQLException {
        
        setUp();
        ps = db.prepareStatement(
            "SELECT V.id FROM " + VINO + " V , " + REFERENCIA + " R WHERE R.codigo = ?");
        ps.setInt(1, idReferencia);
        rs = db.read(ps);
        int idVino = -1;
        if(rs.next())
            idVino = rs.getInt(1);
        tearDown();
        
        return idVino;
    }
    
    public static JsonObject getReferencia(int idReferencia)            
            throws SQLException {
        
        JsonObject referencia = null;
        
        setUp();
        ps = db.prepareStatement(
            "SELECT * FROM " + REFERENCIA + " WHERE codigo = ?");
        ps.setInt(1, idReferencia);
        rs = db.read(ps);
        if(rs.next()){
            referencia = Json.createObjectBuilder()
                    .add("codigo",       idReferencia)
                    .add("esPorCajas",   rs.getString("esPorCajas").equals("T"))
                    .add("contenidoEnCL",rs.getInt("contenidoEnCL"))
                    .add("precio",       rs.getDouble("precio"))
                    .add("disponible",   rs.getString("disponible").equals("T"))
                            .build();
        }
        tearDown();
        
        return referencia;
    }
    
    public static double getPrecioReferencia(int idReferencia)
            throws SQLException {
        
        double precio = 0;
        
        setUp();
        ps = db.prepareStatement(
            "SELECT precio FROM " + REFERENCIA + " WHERE codigo = ?");
        ps.setInt(1, idReferencia);
        rs = db.read(ps);
        if(rs.next())
            precio = rs.getDouble("precio");
        tearDown();
        
        return precio;
    }
    
}
