package presentacion;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import modelo.Abonado;
import modelo.Factura;
import modelo.Pedido;
import utilidades.Fechas;

/**
 * 
 * @author grupo 03 - Diseno de Software
 */
public class ControladorVentanaPerfilResponsableContabilidad {
    
    private VentanaPerfilResponsableContabilidad ventana;
    private List<Factura> facturas;
    
    public ControladorVentanaPerfilResponsableContabilidad(
        VentanaPerfilResponsableContabilidad ventana){
        this.ventana = ventana;
    }
    
    public void eventoConsultarImpagosDesdeFecha()
            throws SQLException {
        Date fechaConsulta = ventana.getFecha();
        if(!Fechas.fechaAnteriorAlMesPasado(fechaConsulta))
            ventana.mensajeFechaMenor30Dias();
        try{
            // Si no hay debería devolver una lista vacía mejor:
            facturas = Factura.getFacturasVencidasAnteriores(fechaConsulta);
            if(facturas == null)
                ventana.mostrarInformeFacturasImpagadas("No se encontraron Facturas impagadas previas a la fecha introducida");
            else
                ventana.mostrarInformeFacturasImpagadas(crearInformeFacturasImpagadas());
        } catch (SQLException e){
            ventana.mostrarMensaje(e.getMessage());
        }
    }
    
    private String crearInformeFacturasImpagadas(){
        
        StringBuffer informeFacturas = new StringBuffer().append("\t==== INFORME DE FACTURAS IMPAGADAS ====\n");
        for(Factura factura : facturas){
            int idFactura = factura.getNumeroFactura();
            try{
                Abonado abonado = Factura.getAbonado(idFactura);
                List<Pedido> pedidos = Factura.getPedidosAsociados(idFactura);
            
                informeFacturas
                        .append("Factura #").append(idFactura).append("\n")
                        .append(" * Abonado: ").append(abonado.getNombre()).append(" ")
                        .append(abonado.getApellidos()).append("\n")
                        .append(" * Formada por los pedidos: \n");
                for(Pedido p : pedidos)
                    informeFacturas.append("    - Pedido #")
                            .append(p.getNumero()).append("\n");
            } catch(SQLException e){
                e.printStackTrace();
            }
        }
        return informeFacturas.toString();
    }
    
    public void eventoBotonSalir(){
        GestorVentanas.mostrarVistaIdentificarse();
        ventana.dispose();
    }
    
}
