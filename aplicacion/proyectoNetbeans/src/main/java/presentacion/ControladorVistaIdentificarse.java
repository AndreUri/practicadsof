package presentacion;

import java.sql.SQLException;
import modelo.controladores.ControladorCUIdentificarse;
import utilidades.excepciones.LoginException;

/**
 * 
 * @author grupo 03 - Diseno de Software
 */
public class ControladorVistaIdentificarse {
    
    VistaIdentificarse ventana;
    ControladorCUIdentificarse fachadaLogin;
    
    public ControladorVistaIdentificarse(VistaIdentificarse ventana){
        this.ventana = ventana;
        fachadaLogin = new ControladorCUIdentificarse();
    }

    public void procesarEventoLogin() {
        String login    = ventana.getLogin();
        String password = ventana.getPassword();
        boolean checkLogin = comprobarObligatorios(login, password);
        
        if(checkLogin){
            try{
                fachadaLogin.getEmpleado(login, password);
                ventana.dispose();
                cargarVista(fachadaLogin.obtenerTipoEmpleado());
            } catch (LoginException | SQLException e) {
                ventana.mostrarMensaje(e.getMessage());
            }
        } else
            ventana.mostrarMensaje("Ningún campo puede quedar vacío");
    }
    
    public boolean comprobarObligatorios(String login, String password){
        if(login.equals("") || password.equals("") || login.equals(null) || password.equals(null))
            return false;
        return true;
    }

    private void cargarVista(char tipoEmpleado) {
        ventana.limpiarCampos();
        switch(tipoEmpleado) {
            case 'A':
                GestorVentanas.mostrarVentanaPerfilResponsableAlmacen();
                break;
            case 'C':
                GestorVentanas.mostrarVentanaPerfilResponsableContabilidad();
                break;
            case 'P':
                GestorVentanas.mostrarVentanaPerfilResponsablePedidos();
                break;
        }
    }
    
    public void eventoBotonCancelar(){
        ventana.dispose();
    }
    
    public void eventoBotonCerrar(){
        ventana.dispose();
    }
}
