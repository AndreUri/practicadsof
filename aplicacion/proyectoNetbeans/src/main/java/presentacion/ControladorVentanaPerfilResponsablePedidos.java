package presentacion;
import java.sql.SQLException;
import modelo.controladores.ControladorCUProcesarPedido;
import utilidades.excepciones.AbonadoException;
import utilidades.excepciones.AbonadoConPlazosPendientesException;
import utilidades.excepciones.ReferenciaException;

/**
 * 
 * @author grupo 03 - Diseno de Software
 */
public class ControladorVentanaPerfilResponsablePedidos {
    
    private VentanaPerfilResponsablePedidos ventana;
    private ControladorCUProcesarPedido fachadaPedido;
    
    public ControladorVentanaPerfilResponsablePedidos(
            VentanaPerfilResponsablePedidos ventana){
        this.ventana = ventana;
        this.fachadaPedido = new ControladorCUProcesarPedido();
        ventana.iniciarVista();
    }
    
    public void eventoBotonConfirmarDatosAbonado(){
        try{
            fachadaPedido.crearPedido();
            ventana.cambiarVistaAlComfirmarAbonado();
        } catch(AbonadoConPlazosPendientesException | SQLException e){
            ventana.mostrarMensaje(e.getMessage());
            eventoBotonNoConfirmarDatosAbonado();
        }        
    }
    
    public void eventoBotonNoConfirmarDatosAbonado(){
        ventana.cambiarVistaAlNoComfirmarAbonado();
        fachadaPedido.eliminarAbonado();
    }
    
    public void eventoBotonProcesarPedidos(){
        ventana.mostrarPanelIntroducirID();
    }
    
    public void eventoBotonProcesarAbonado(){
        try{
            int numeroAbonado = Integer.parseInt(ventana.getNumeroAbonado());
            
            fachadaPedido.introducirAbonado(numeroAbonado);
            
            ventana.mostrarJLabelDatosAbonado();
            ventana.ocultarProcesarIDYBuscar();
            ventana.mostrarDatosDelAbonado(fachadaPedido.getAbonado());
            
        }catch(AbonadoException | SQLException e){
            ventana.mostrarMensaje(e.getMessage());
            ventana.limpiarIdAbonado();
        }catch(NumberFormatException e){
            ventana.mostrarMensaje("Introducir un número de abonado válido");
            ventana.limpiarIdAbonado();
        }
    }
    
    public void eventoBotonAnadirLineaPedido(){
        try{
            
            Integer referencia = Integer.parseInt(ventana.getReferencia());
            Integer cantidad   = Integer.parseInt(ventana.getCantidad());
            
            comprobarCantidadPositiva(cantidad);
            ventana.limpiarCamposRefYCant();
            
            fachadaPedido.addLinea(referencia, cantidad);
            // y el CU debe continuar...
        }catch(NumberFormatException e){
            ventana.mensajeInputIncorrecta("La cantidad de la línea no puede ser negativa");
        }catch(ReferenciaException | SQLException e){
            ventana.mensajeReferenciaException(e.getMessage());
        }
    }
    
    public void eventoBotonTerminarPedido() {
        try {
            fachadaPedido.finalizarPedido();
        } catch (SQLException ex) {
            // TODO: usar un mensaje un poco más adecuado
            ventana.mensajeInputIncorrecta("NO SE PUDO GUARDAR EL PEDIDO");
        }
        GestorVentanas.mostrarVentanaPerfilResponsablePedidos();
        ventana.dispose();
    } 
    
    public void eventoBotonSalir(){
        fachadaPedido.cancelar();
        GestorVentanas.mostrarVistaIdentificarse();
        ventana.dispose();
    }
    
    private void comprobarCantidadPositiva(int entrada){
        if(entrada < 0) throw new NumberFormatException();
    }
    
    public void eventoBotonCancelar(){
        fachadaPedido.cancelar();
        GestorVentanas.mostrarVentanaPerfilResponsablePedidos();
        ventana.dispose();
    }
    
}