package presentacion;

import java.sql.SQLException;
import java.util.List;
import modelo.LineaCompra;
import utilidades.excepciones.CompraException;
import modelo.controladores.ControladorCURegistrarCompra;
import utilidades.excepciones.LineasNoRecibidasException;

/**
 * 
 * @author grupo 03 - Diseno de Software
 */
public class ControladorVentanaPerfilResponsableAlmacen {
    
    final private VentanaPerfilResponsableAlmacen ventana;
    private final ControladorCURegistrarCompra fachadaCompra;
    
    public ControladorVentanaPerfilResponsableAlmacen(VentanaPerfilResponsableAlmacen ventana){
        fachadaCompra = new ControladorCURegistrarCompra();
        this.ventana = ventana;
        ventana.iniciarVista();
    }
    
    public void eventoBotonRegistrarCompraRecibida(){
        ventana.mostrarPanelIntroducirID();
    }
    
    public void eventoBotonSalir(){
        fachadaCompra.cancelar();
        GestorVentanas.mostrarVistaIdentificarse();
        ventana.dispose();
    }
    
    public void eventoBotonContinuar(){
        try{
            int id = Integer.parseInt(ventana.getIdCompra());
            fachadaCompra.getCompraRecibida(id);
            ventana.ocultarBotonesIntroducirID();
            ventana.mostrarPanelCompra();
            ventana.mostrarBotonFinalizar();
            ventana.mostrarNombreBodega(fachadaCompra.getBodega());
            ventana.mostrarLineasCompra(fachadaCompra.getLineasNoRecibidas());
        } catch(CompraException | SQLException e){
            ventana.mostrarMensaje(e.getMessage());
        } catch(NumberFormatException e){
            ventana.mensajeInputIncorrecta();
         }
        
    }
    
    public void eventoBotonCompletarLineaPedido(){
        if(ventana.saberLineaSeleccionada() != -1){
            // TODO: fixme
            int id = fachadaCompra.getLineaDesdeReferencia(ventana.getRefLineaSeleccionada()).getId();
            fachadaCompra.recibirLinea(id);
            ventana.eliminarLineaSeleccionada();
        }
    }
   
    public void eventoBotonCancelar(){
        fachadaCompra.cancelar();
        ventana.iniciarVista();
    }
    
    public void eventoBotonCompletarFinal(){
        try {
            fachadaCompra.finalizarSeleccion();
        } catch (LineasNoRecibidasException e){
            ventana.iniciarDialogoComprasIncorrectas();
            List<LineaCompra> lineasNoRecibidas = (List<LineaCompra>) e.lineasNoRecibidas;
            ventana.mostrarLineasCompraNoCorrectas(lineasNoRecibidas);
        } catch (SQLException e){
            ventana.mostrarMensaje(e.getMessage());
        }
    }

    public void eventoBotonOk() {
        ventana.destruirDialogoComprasIncorrectas();
        ventana.iniciarVista();
    }
    
}
