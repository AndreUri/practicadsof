package vinoteca.modelo.clasesDominio;

import java.sql.Date;
import java.util.Currency;

/**
 * 
 * @author grupo 03 - Diseno de Software
 */
public class Factura {
  private int numeroFactura;
  private Date fechaEmision;
  private Currency importe;
  private EstadoFactura estado;
  private Date fechaPago; //[0..1]
  private String idExtractoBancario;

    public Factura(int numeroFactura, Date fechaEmision, Currency importe, EstadoFactura estado, Date fechaPago, String idExtractoBancario) {
        this.numeroFactura = numeroFactura;
        this.fechaEmision = fechaEmision;
        this.importe = importe;
        this.estado = estado;
        this.fechaPago = fechaPago;
        this.idExtractoBancario = idExtractoBancario;
    }

    /**
     * @return the numeroFactura
     */
    public int getNumeroFactura() {
        return numeroFactura;
    }

    /**
     * @param numeroFactura the numeroFactura to set
     */
    public void setNumeroFactura(int numeroFactura) {
        this.numeroFactura = numeroFactura;
    }

    /**
     * @return the fechaEmision
     */
    public Date getFechaEmision() {
        return fechaEmision;
    }

    /**
     * @param fechaEmision the fechaEmision to set
     */
    public void setFechaEmision(Date fechaEmision) {
        this.fechaEmision = fechaEmision;
    }

    /**
     * @return the importe
     */
    public Currency getImporte() {
        return importe;
    }

    /**
     * @param importe the importe to set
     */
    public void setImporte(Currency importe) {
        this.importe = importe;
    }

    /**
     * @return the estado
     */
    public EstadoFactura getEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(EstadoFactura estado) {
        this.estado = estado;
    }

    /**
     * @return the fechaPago
     */
    public Date getFechaPago() {
        return fechaPago;
    }

    /**
     * @param fechaPago the fechaPago to set
     */
    public void setFechaPago(Date fechaPago) {
        this.fechaPago = fechaPago;
    }

    /**
     * @return the idExtractoBancario
     */
    public String getIdExtractoBancario() {
        return idExtractoBancario;
    }

    /**
     * @param idExtractoBancario the idExtractoBancario to set
     */
    public void setIdExtractoBancario(String idExtractoBancario) {
        this.idExtractoBancario = idExtractoBancario;
    }
}
