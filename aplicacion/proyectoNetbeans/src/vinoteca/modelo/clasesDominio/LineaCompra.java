package vinoteca.modelo.clasesDominio;

import java.sql.Date;

/**
 * 
 * @author grupo 03 - Diseno de Software
 */
public class LineaCompra {
    private int unidades;
    private boolean recibida;
    private Date fechaRecepcion;

    public LineaCompra(int unidades, boolean recibida, Date fechaRecepcion) {
        this.unidades = unidades;
        this.recibida = recibida;
        this.fechaRecepcion = fechaRecepcion;
    }

    /**
     * @return the unidades
     */
    public int getUnidades() {
        return unidades;
    }

    /**
     * @param unidades the unidades to set
     */
    public void setUnidades(int unidades) {
        this.unidades = unidades;
    }

    /**
     * @return the recibida
     */
    public boolean isRecibida() {
        return recibida;
    }

    /**
     * @param recibida the recibida to set
     */
    public void setRecibida(boolean recibida) {
        this.recibida = recibida;
    }

    /**
     * @return the fechaRecepcion
     */
    public Date getFechaRecepcion() {
        return fechaRecepcion;
    }

    /**
     * @param fechaRecepcion the fechaRecepcion to set
     */
    public void setFechaRecepcion(Date fechaRecepcion) {
        this.fechaRecepcion = fechaRecepcion;
    }
}
