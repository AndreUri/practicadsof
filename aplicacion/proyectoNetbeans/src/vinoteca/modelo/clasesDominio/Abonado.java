package vinoteca.modelo.clasesDominio;

/**
 * 
 * @author grupo 03 - Diseno de Software
 */
public class Abonado extends Persona{
    private int numeroAbonado;
    private int openIDref; //¿?
    
    /**
     * @return the numeroAbonado
     */
    public int getNumeroAbonado() {
        return numeroAbonado;
    }

    /**
     * @param numeroAbonado the numeroAbonado to set
     */
    public void setNumeroAbonado(int numeroAbonado) {
        this.numeroAbonado = numeroAbonado;
    }

    /**
     * @return the openIDref
     */
    public int getOpenIDref() {
        return openIDref;
    }

    /**
     * @param openIDref the openIDref to set
     */
    public void setOpenIDref(int openIDref) {
        this.openIDref = openIDref;
    }
    
    
    
}
