package vinoteca.modelo.clasesDominio;

import java.sql.Date;
import java.util.Currency;

/**
 * 
 * @author grupo 03 - Diseno de Software
 */
public class Compra {
    private int idCompra;
    private Date fechaInicioCompra;
    private boolean recibidaCompleta;
    private Date fechaCompraCompletada; //[0..1]
    private Currency importe;
    private boolean pagada;
    private Date fechaPago; //[0..1]

    public Compra(int idCompra, Date fechaInicioCompra, boolean recibidaCompleta, Date fechaCompraCompletada, Currency importe, boolean pagada, Date fechaPago) {
        this.idCompra = idCompra;
        this.fechaInicioCompra = fechaInicioCompra;
        this.recibidaCompleta = recibidaCompleta;
        this.fechaCompraCompletada = fechaCompraCompletada;
        this.importe = importe;
        this.pagada = pagada;
        this.fechaPago = fechaPago;
    }

    /**
     * @return the idCompra
     */
    public int getIdCompra() {
        return idCompra;
    }

    /**
     * @param idCompra the idCompra to set
     */
    public void setIdCompra(int idCompra) {
        this.idCompra = idCompra;
    }

    /**
     * @return the fechaInicioCompra
     */
    public Date getFechaInicioCompra() {
        return fechaInicioCompra;
    }

    /**
     * @param fechaInicioCompra the fechaInicioCompra to set
     */
    public void setFechaInicioCompra(Date fechaInicioCompra) {
        this.fechaInicioCompra = fechaInicioCompra;
    }

    /**
     * @return the recibidaCompleta
     */
    public boolean isRecibidaCompleta() {
        return recibidaCompleta;
    }

    /**
     * @param recibidaCompleta the recibidaCompleta to set
     */
    public void setRecibidaCompleta(boolean recibidaCompleta) {
        this.recibidaCompleta = recibidaCompleta;
    }

    /**
     * @return the fechaCompraCompletada
     */
    public Date getFechaCompraCompletada() {
        return fechaCompraCompletada;
    }

    /**
     * @param fechaCompraCompletada the fechaCompraCompletada to set
     */
    public void setFechaCompraCompletada(Date fechaCompraCompletada) {
        this.fechaCompraCompletada = fechaCompraCompletada;
    }

    /**
     * @return the importe
     */
    public Currency getImporte() {
        return importe;
    }

    /**
     * @param importe the importe to set
     */
    public void setImporte(Currency importe) {
        this.importe = importe;
    }

    /**
     * @return the pagada
     */
    public boolean isPagada() {
        return pagada;
    }

    /**
     * @param pagada the pagada to set
     */
    public void setPagada(boolean pagada) {
        this.pagada = pagada;
    }

    /**
     * @return the fechaPago
     */
    public Date getFechaPago() {
        return fechaPago;
    }

    /**
     * @param fechaPago the fechaPago to set
     */
    public void setFechaPago(Date fechaPago) {
        this.fechaPago = fechaPago;
    }
    
}
