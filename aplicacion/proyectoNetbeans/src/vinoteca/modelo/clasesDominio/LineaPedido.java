package vinoteca.modelo.clasesDominio;

/**
 * 
 * @author grupo 03 - Diseno de Software
 */
public class LineaPedido {
    private int unidades;
    private boolean completada;

    public LineaPedido(int unidades, boolean completada) {
        this.unidades = unidades;
        this.completada = completada;
    }
    
    /**
     * @return the unidades
     */
    public int getUnidades() {
        return unidades;
    }

    /**
     * @param unidades the unidades to set
     */
    public void setUnidades(int unidades) {
        this.unidades = unidades;
    }

    /**
     * @return the completada
     */
    public boolean isCompletada() {
        return completada;
    }

    /**
     * @param completada the completada to set
     */
    public void setCompletada(boolean completada) {
        this.completada = completada;
    }
}
