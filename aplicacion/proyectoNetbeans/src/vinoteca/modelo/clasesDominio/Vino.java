package vinoteca.modelo.clasesDominio;

/**
 * 
 * @author grupo 03 - Diseno de Software
 */
public class Vino {
    private String nombreComercial;
    private int ano;
    private DenominacionOrigen denominacion;
    private Categoria categoria;
    private String comentario;

    public Vino(String nombreComercial, int ano, DenominacionOrigen denominacion, Categoria categoria, String comentario) {
        this.nombreComercial = nombreComercial;
        this.ano = ano;
        this.denominacion = denominacion;
        this.categoria = categoria;
        this.comentario = comentario;
    }
    
    /**
     * @return the nombreComercial
     */
    public String getNombreComercial() {
        return nombreComercial;
    }

    /**
     * @param nombreComercial the nombreComercial to set
     */
    public void setNombreComercial(String nombreComercial) {
        this.nombreComercial = nombreComercial;
    }

    /**
     * @return the ano
     */
    public int getAno() {
        return ano;
    }

    /**
     * @param ano the ano to set
     */
    public void setAno(int ano) {
        this.ano = ano;
    }

    /**
     * @return the denominacion
     */
    public DenominacionOrigen getDenominacion() {
        return denominacion;
    }

    /**
     * @param denominacion the denominacion to set
     */
    public void setDenominacion(DenominacionOrigen denominacion) {
        this.denominacion = denominacion;
    }

    /**
     * @return the categoria
     */
    public Categoria getCategoria() {
        return categoria;
    }

    /**
     * @param categoria the categoria to set
     */
    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    /**
     * @return the comentario
     */
    public String getComentario() {
        return comentario;
    }

    /**
     * @param comentario the comentario to set
     */
    public void setComentario(String comentario) {
        this.comentario = comentario;
    }
}
