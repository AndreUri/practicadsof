package vinoteca.modelo.clasesDominio;

import java.sql.Date;

/**
 * 
 * @author grupo 03 - Diseno de Software
 */
public class Empleado extends Persona{

    private String login;
    private String password;
    private Date fechaInicio;
    private char tipoEmpleado;

    public Empleado(String login, String password, Date fechaInicio, char tipoEmpleado) {
        this.login = login;
        this.password = password;
        this.fechaInicio = fechaInicio;
        this.tipoEmpleado = tipoEmpleado;
    }
    
    /**
     * @return the login
     */
    public String getLogin() {
        return login;
    }

    /**
     * @param login the login to set
     */
    public void setLogin(String login) {
        this.login = login;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the fechaInicio
     */
    public Date getFechaInicio() {
        return fechaInicio;
    }

    /**
     * @param fechaInicio the fechaInicio to set
     */
    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    /**
     * @return the tipoEmpleado
     */
    public char getTipoEmpleado() {
        return tipoEmpleado;
    }
    
    public static Empleado getEmpleado(String login) {
        Empleado e = null/*GestorPersistenciaEmpleado.recuperaEmpleado(login, password)*/;
        return e;
    }
}
