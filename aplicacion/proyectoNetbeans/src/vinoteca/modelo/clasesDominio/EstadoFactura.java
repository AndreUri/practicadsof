package vinoteca.modelo.clasesDominio;

/**
 * 
 * @author grupo 03 - Diseno de Software
 */
public enum EstadoFactura {
    EMITIDA,
    VENCIDA,
    PAGADA;
}