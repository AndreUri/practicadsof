package vinoteca.modelo.clasesDominio;

import java.util.Currency;

/**
 * 
 * @author grupo 03 - Diseno de Software
 */
public class Referencia {
    private int codigo;
    private boolean esPorCajas;
    private int contenidoCL;
    private Currency precio;
    private boolean disponible;

    public Referencia(int codigo, boolean esPorCajas, int contenidoCL, Currency precio, boolean disponible) {
        this.codigo = codigo;
        this.esPorCajas = esPorCajas;
        this.contenidoCL = contenidoCL;
        this.precio = precio;
        this.disponible = disponible;
    }

    /**
     * @return the codigo
     */
    public int getCodigo() {
        return codigo;
    }

    /**
     * @param codigo the codigo to set
     */
    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    /**
     * @return the esPorCajas
     */
    public boolean isEsPorCajas() {
        return esPorCajas;
    }

    /**
     * @param esPorCajas the esPorCajas to set
     */
    public void setEsPorCajas(boolean esPorCajas) {
        this.esPorCajas = esPorCajas;
    }

    /**
     * @return the contenidoCL
     */
    public int getContenidoCL() {
        return contenidoCL;
    }

    /**
     * @param contenidoCL the contenidoCL to set
     */
    public void setContenidoCL(int contenidoCL) {
        this.contenidoCL = contenidoCL;
    }

    /**
     * @return the precio
     */
    public Currency getPrecio() {
        return precio;
    }

    /**
     * @param precio the precio to set
     */
    public void setPrecio(Currency precio) {
        this.precio = precio;
    }

    /**
     * @return the disponible
     */
    public boolean isDisponible() {
        return disponible;
    }

    /**
     * @param disponible the disponible to set
     */
    public void setDisponible(boolean disponible) {
        this.disponible = disponible;
    }
}
