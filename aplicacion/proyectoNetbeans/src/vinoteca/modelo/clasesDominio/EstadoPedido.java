package vinoteca.modelo.clasesDominio;

/**
 * 
 * @author grupo 03 - Diseno de Software
 */
public enum EstadoPedido {
    PENDIENTE,
    TRAMITADO,
    COMPLETADO,
    SERVICIO,
    FACTURADO,
    PAGADO;
}
