package vinoteca.modelo.clasesDominio;

import java.sql.Date;
import java.util.Currency;

/**
 * 
 * @author grupo 03 - Diseno de Software
 */
public class Pedido {
    private int numero;
    private EstadoPedido estado;
    private Date fechaRealizacion;
    private String notaEntrega;
    private Currency importe;
    private Date fechaRecepcion; //[0..1]
    private Date fechaEntrega; //[0..1]

    public Pedido(int numero, EstadoPedido estado, Date fechaRealizacion, String notaEntrega, Currency importe, Date fechaRecepcion, Date fechaEntrega) {
        this.numero = numero;
        this.estado = estado;
        this.fechaRealizacion = fechaRealizacion;
        this.notaEntrega = notaEntrega;
        this.importe = importe;
        this.fechaRecepcion = fechaRecepcion;
        this.fechaEntrega = fechaEntrega;
    }

    /**
     * @return the numero
     */
    public int getNumero() {
        return numero;
    }

    /**
     * @param numero the numero to set
     */
    public void setNumero(int numero) {
        this.numero = numero;
    }

    /**
     * @return the estado
     */
    public EstadoPedido getEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(EstadoPedido estado) {
        this.estado = estado;
    }

    /**
     * @return the fechaRealizacion
     */
    public Date getFechaRealizacion() {
        return fechaRealizacion;
    }

    /**
     * @param fechaRealizacion the fechaRealizacion to set
     */
    public void setFechaRealizacion(Date fechaRealizacion) {
        this.fechaRealizacion = fechaRealizacion;
    }

    /**
     * @return the notaEntrega
     */
    public String getNotaEntrega() {
        return notaEntrega;
    }

    /**
     * @param notaEntrega the notaEntrega to set
     */
    public void setNotaEntrega(String notaEntrega) {
        this.notaEntrega = notaEntrega;
    }

    /**
     * @return the importe
     */
    public Currency getImporte() {
        return importe;
    }

    /**
     * @param importe the importe to set
     */
    public void setImporte(Currency importe) {
        this.importe = importe;
    }

    /**
     * @return the fechaRecepcion
     */
    public Date getFechaRecepcion() {
        return fechaRecepcion;
    }

    /**
     * @param fechaRecepcion the fechaRecepcion to set
     */
    public void setFechaRecepcion(Date fechaRecepcion) {
        this.fechaRecepcion = fechaRecepcion;
    }

    /**
     * @return the fechaEntrega
     */
    public Date getFechaEntrega() {
        return fechaEntrega;
    }

    /**
     * @param fechaEntrega the fechaEntrega to set
     */
    public void setFechaEntrega(Date fechaEntrega) {
        this.fechaEntrega = fechaEntrega;
    }
}
