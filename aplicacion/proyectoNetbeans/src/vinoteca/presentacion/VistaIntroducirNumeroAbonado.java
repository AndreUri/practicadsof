package vinoteca.presentacion;

/**
 * 
 * @author grupo 03 - Diseno de Software
 */
public class VistaIntroducirNumeroAbonado extends javax.swing.JFrame {

    /**
     * Creates new form VistaIntroducirNumeroAbonado
     */
    public VistaIntroducirNumeroAbonado() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        procesarPedidosLabel = new javax.swing.JPanel();
        tituloProcesarPedidos = new javax.swing.JLabel();
        introducirNumeroAbonado = new javax.swing.JLabel();
        numeroAbonado = new javax.swing.JTextField();
        buscarAbonado = new javax.swing.JCheckBox();
        procesarPedido = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        tituloProcesarPedidos.setFont(new java.awt.Font("Lucida Grande", 0, 24)); // NOI18N
        tituloProcesarPedidos.setText("Procesar Pedidos");

        introducirNumeroAbonado.setFont(new java.awt.Font("Lucida Grande", 0, 16)); // NOI18N
        introducirNumeroAbonado.setText("Introduzca el número de abonado:");

        buscarAbonado.setText("Buscar abonado");
        buscarAbonado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buscarAbonadoActionPerformed(evt);
            }
        });

        procesarPedido.setText("Procesar");

        javax.swing.GroupLayout procesarPedidosLabelLayout = new javax.swing.GroupLayout(procesarPedidosLabel);
        procesarPedidosLabel.setLayout(procesarPedidosLabelLayout);
        procesarPedidosLabelLayout.setHorizontalGroup(
            procesarPedidosLabelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(procesarPedidosLabelLayout.createSequentialGroup()
                .addGroup(procesarPedidosLabelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(procesarPedidosLabelLayout.createSequentialGroup()
                        .addGap(14, 14, 14)
                        .addComponent(tituloProcesarPedidos))
                    .addGroup(procesarPedidosLabelLayout.createSequentialGroup()
                        .addGap(44, 44, 44)
                        .addGroup(procesarPedidosLabelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(procesarPedidosLabelLayout.createSequentialGroup()
                                .addComponent(buscarAbonado)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(procesarPedido))
                            .addComponent(introducirNumeroAbonado, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(numeroAbonado))))
                .addContainerGap(85, Short.MAX_VALUE))
        );
        procesarPedidosLabelLayout.setVerticalGroup(
            procesarPedidosLabelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(procesarPedidosLabelLayout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addComponent(tituloProcesarPedidos)
                .addGap(18, 18, 18)
                .addComponent(introducirNumeroAbonado)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(numeroAbonado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(procesarPedidosLabelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(buscarAbonado, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(procesarPedido))
                .addContainerGap(93, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(procesarPedidosLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(procesarPedidosLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void buscarAbonadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buscarAbonadoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_buscarAbonadoActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(VistaIntroducirNumeroAbonado.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(VistaIntroducirNumeroAbonado.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(VistaIntroducirNumeroAbonado.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(VistaIntroducirNumeroAbonado.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new VistaIntroducirNumeroAbonado().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JCheckBox buscarAbonado;
    private javax.swing.JLabel introducirNumeroAbonado;
    private javax.swing.JTextField numeroAbonado;
    private javax.swing.JButton procesarPedido;
    private javax.swing.JPanel procesarPedidosLabel;
    private javax.swing.JLabel tituloProcesarPedidos;
    // End of variables declaration//GEN-END:variables
}
