package vinoteca.presentacion;

import java.util.Date;
import vinoteca.modelo.clasesDominio.EstadoFactura;
import vinoteca.modelo.clasesDominio.Factura;

/**
 * 
 * @author grupo 03 - Diseno de Software
 */
public class ControladorVentanaPerfilResponsableContabilidad {
    
    private VentanaPerfilResponsableContabilidad ventana;
    
    public ControladorVentanaPerfilResponsableContabilidad(
            VentanaPerfilResponsableContabilidad ventana){
                this.ventana = ventana;
    }
    
    public void eventoBotonSalir(){
        GestorVentanas.mostrarVistaIdentificarse();
        ventana.dispose();
    }
    
    //revisar la excepcion para que si no es una fecha pase algo.
    public void eventoBotonContinuar(){
        try{
            
            //Llamar al controlador del caso de uso para que devuelva el array con las facturas 
            ventana.mostrarFacturasImpagadas(null);
            
        }catch(NumberFormatException e){
            ventana.mensajeFechaMenor30Dias();
            /*esto hay que sustituirlo por su correspondiente mensaje*/
        }
    }
}
