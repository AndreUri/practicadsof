package vinoteca.presentacion;

/**
 * 
 * @author grupo 03 - Diseno de Software
 */
public class ControladorVentanaPerfilResponsablePedidos {
    
    private VentanaPerfilResponsablePedidos ventana;
    
    public ControladorVentanaPerfilResponsablePedidos(
            VentanaPerfilResponsablePedidos ventana){
        this.ventana = ventana;
    }
    
    public void eventoBotonProcesarAbonado() throws NumberFormatException {
        try{
            int numeroAbonado = Integer.parseInt(ventana.getNumeroAbonado());
            comprobarEntradaMayor1(numeroAbonado);
            ventana.mostrarJLabelDatosAbonado();
            ventana.ocultarProcesarIDYBuscar();
        }catch(NumberFormatException e){
            ventana.mensajeInputIncorrecta("numero Abonado");
        }
    }
    
    public void eventoBotonAnadirPedido()throws NumberFormatException{
        try{
            int referencia = Integer.parseInt(ventana.getReferencia());
            int cantidad = Integer.parseInt(ventana.getCantidad());
        }catch(NumberFormatException e){
            ventana.mensajeInputIncorrecta("referencia o cantidad");
        }
    }
    
    public void eventoBotonTerminarPedido(){
           //Hablar con el CtrlCUso para que registre el pedido
        GestorVentanas.mostrarVentanaPerfilResponsablePedidos();
        ventana.dispose();
    } 
    
    public void eventoBotonSalir(){
        GestorVentanas.mostrarVistaIdentificarse();
        ventana.dispose();
    }
    
    private void comprobarEntradaMayor1(int entrada){
        if(entrada < 0){
            throw new NumberFormatException();
        }
    }
        
    
}
