/*
 * --------------------------------------------------------
 * 	  PRÁCTICA DISEÑO DE SOFTWARE - CURSO 2014/2015
 * Gº Ingeniería Informática - Universidad de Valladolid
 * --------------------------------------------------------
 *
 * Script para la creación de la BDD derby de la aplicación
 * 
 * @author --- GRUPO 03 ---
 * @author Samuel Alfageme
 * @author Pablo Carrascal
 * @author Andrea Escribano
 */

-- Usuario y Password no serían estrictamente necesarios.
connect 'jdbc:derby:PracticaDSOF;create=true;user=usuario;password=pass';
run 'PracticaDSOF.sql';
exit;
