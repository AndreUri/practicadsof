DROP TABLE LineaPedido;
DROP TABLE Referencia;
DROP TABLE Preferencia;
DROP TABLE Pedido;
DROP TABLE Empleado;
DROP TABLE Abonado;
DROP TABLE Vino;
DROP TABLE Persona;
DROP TABLE LineaCompra;
DROP TABLE Factura;
DROP TABLE EstadoPedido;
DROP TABLE EstadoFactura;
DROP TABLE DenominacionOrigen;
DROP TABLE Compra;
DROP TABLE Categoria;
DROP TABLE Bodega;


CREATE TABLE Bodega (
 id INT NOT NULL,
 nombre VARCHAR(50),
 CIF VARCHAR(9),
 direccion VARCHAR(50)
);

ALTER TABLE Bodega ADD CONSTRAINT PK_Bodega PRIMARY KEY (id);


CREATE TABLE Categoria (
 clave CHAR(1) NOT NULL 
 	CHECK(	clave = 'A' OR -- Del Año
 			clave = 'C' OR -- Crianza
 			clave = 'R' OR -- Reserva
 			clave = 'G'),  -- Gran Reserva
 nombre VARCHAR(20)
);

ALTER TABLE Categoria ADD CONSTRAINT PK_Categoria PRIMARY KEY (clave);


CREATE TABLE Compra (
 idCompra INT NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),
 fechaInicioCompra DATE NOT NULL,
 recibidaCompleta CHAR(1)
 DEFAULT 'F'
 CHECK( recibidaCompleta = 'T' OR   -- True
		recibidaCompleta = 'F'),	-- False
 fechaCompraCompletada DATE,
 importe REAL,
 pagada CHAR(1)
 DEFAULT 'F'
 CHECK( pagada = 'T' OR  -- True
		pagada = 'F'),	 -- False
 fechaPago DATE
);

ALTER TABLE Compra ADD CONSTRAINT PK_Compra PRIMARY KEY (idCompra);


CREATE TABLE DenominacionOrigen (
 id INT NOT NULL,
 nombre VARCHAR(50)
);

ALTER TABLE DenominacionOrigen ADD CONSTRAINT PK_DenominacionOrigen PRIMARY KEY (id);


CREATE TABLE EstadoFactura (
 clave CHAR(1) NOT NULL 
 	CHECK(	clave = 'E' OR -- Emitida
 			clave = 'V' OR -- Vencida
 			clave = 'P'),  -- Pagada
 nombre VARCHAR(20)
);

ALTER TABLE EstadoFactura ADD CONSTRAINT PK_EstadoFactura PRIMARY KEY (clave);


CREATE TABLE EstadoPedido (
 clave CHAR(1) NOT NULL
  	CHECK(	clave = 'P' OR -- Pendiente
 			clave = 'T' OR -- Tramitado
 			clave = 'C' OR -- Completado
 			clave = 'S' OR -- Servido
 			clave = 'F' OR -- Facturado
 			clave = 'A'),  -- Abonado / Pagado
 nombre VARCHAR(20)
);

ALTER TABLE EstadoPedido ADD CONSTRAINT PK_EstadoPedido PRIMARY KEY (clave);


CREATE TABLE Factura (
 numeroFactura INT NOT NULL,
 fechaEmision DATE NOT NULL,
 importe REAL,
 estado CHAR(1) NOT NULL,
 fechaPago DATE,
 idExtractoBancario VARCHAR(30) NOT NULL
);

ALTER TABLE Factura ADD CONSTRAINT PK_Factura PRIMARY KEY (numeroFactura);


CREATE TABLE LineaCompra (
 id INT NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),
 unidades SMALLINT,
 recibida CHAR(1)
 DEFAULT 'F'
 CHECK( recibida = 'T' OR   -- True
		recibida = 'F'),	-- False
 fechaRecepcion DATE,
 idCompra INT NOT NULL
);

ALTER TABLE LineaCompra ADD CONSTRAINT PK_LineaCompra PRIMARY KEY (id);


CREATE TABLE Persona (
 nif 			VARCHAR(9) NOT NULL,
 nombre 		VARCHAR(50),
 apellidos 		VARCHAR(50),
 direccion 		VARCHAR(50),
 telefono 		VARCHAR(50),
 email 			VARCHAR(50),
 cuentaBancaria VARCHAR(30) -- PUEDE SER NULL
);

ALTER TABLE Persona ADD CONSTRAINT PK_Persona PRIMARY KEY (nif);


CREATE TABLE Vino (
 id INT NOT NULL,
 nombreComercial VARCHAR(50),
 año SMALLINT,
 comentario VARCHAR(200),
 idDenominacion INT NOT NULL,
 categoria CHAR(1) NOT NULL,
 idBodega INT NOT NULL
);

ALTER TABLE Vino ADD CONSTRAINT PK_Vino PRIMARY KEY (id);


CREATE TABLE Abonado (
 nif VARCHAR(9) NOT NULL,
 numeroAbonado INT NOT NULL UNIQUE,
 openIDref VARCHAR(50) UNIQUE
);

ALTER TABLE Abonado ADD CONSTRAINT PK_Abonado PRIMARY KEY (nif,numeroAbonado);


CREATE TABLE Empleado (
 nif VARCHAR(9) NOT NULL,
 login VARCHAR(20) NOT NULL UNIQUE,
 password VARCHAR(20) NOT NULL,
 fechaInicio DATE,
 tipoEmpleado CHAR(1)
);

ALTER TABLE Empleado ADD CONSTRAINT PK_Empleado PRIMARY KEY (nif,login);


CREATE TABLE Pedido (
 numero INT NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),
 estado CHAR(1) NOT NULL
 DEFAULT 'P',				-- Los Pedidos se crean Pendientes
 fechaRealizacion DATE,
 notaEntrega VARCHAR(200),
 importe REAL
 DEFAULT 0.0,
 fechaRecepcion DATE,
 fechaEntrega DATE,
 nif VARCHAR(9) NOT NULL,
 numeroAbonado INT NOT NULL,
 numeroFactura INT  -- NOT NULL : la factura debería crearse a posteriori
);

ALTER TABLE Pedido ADD CONSTRAINT PK_Pedido PRIMARY KEY (numero);


CREATE TABLE Preferencia (
 idDenominacion INT NOT NULL,
 categoria CHAR(1) NOT NULL,
 nif VARCHAR(9) NOT NULL,
 numeroAbonado INT NOT NULL
);


CREATE TABLE Referencia (
 codigo INT NOT NULL,
 esPorCajas CHAR(1)
 CHECK(	esPorCajas = 'T' OR -- True
 		esPorCajas = 'F'),	-- False
 contenidoEnCL SMALLINT,
 precio REAL,
 disponible CHAR(1)
 CHECK(	disponible = 'T' OR -- True
 		disponible = 'F'),	-- False
 vinoid INT NOT NULL
);

ALTER TABLE Referencia ADD CONSTRAINT PK_Referencia PRIMARY KEY (codigo);

CREATE TABLE LineaPedido (
 id INT NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),
 unidades INT,
 completada CHAR(1)
 DEFAULT 'F'				-- Las líneas de pedido se crean sin estar completadas
 CHECK(	completada = 'T' OR -- True
		completada = 'F'),	-- False
 codigo INT NOT NULL,
 idLineaCompra INT,
 numero INT NOT NULL		-- Renombrar
);

ALTER TABLE LineaPedido ADD CONSTRAINT PK_LineaPedido PRIMARY KEY (id);


ALTER TABLE Factura ADD CONSTRAINT FK_Factura_0 FOREIGN KEY (estado) REFERENCES EstadoFactura (clave);


ALTER TABLE LineaCompra ADD CONSTRAINT FK_LineaCompra_0 FOREIGN KEY (idCompra) REFERENCES Compra (idCompra);


ALTER TABLE Vino ADD CONSTRAINT FK_Vino_0 FOREIGN KEY (idDenominacion) REFERENCES DenominacionOrigen (id);
ALTER TABLE Vino ADD CONSTRAINT FK_Vino_1 FOREIGN KEY (categoria) REFERENCES Categoria (clave);
ALTER TABLE Vino ADD CONSTRAINT FK_Vino_2 FOREIGN KEY (idBodega) REFERENCES Bodega (id);


ALTER TABLE Abonado ADD CONSTRAINT FK_Abonado_0 FOREIGN KEY (nif) REFERENCES Persona (nif);


ALTER TABLE Empleado ADD CONSTRAINT FK_Empleado_0 FOREIGN KEY (nif) REFERENCES Persona (nif);


ALTER TABLE Pedido ADD CONSTRAINT FK_Pedido_0 FOREIGN KEY (estado) REFERENCES EstadoPedido (clave);
ALTER TABLE Pedido ADD CONSTRAINT FK_Pedido_1 FOREIGN KEY (nif,numeroAbonado) REFERENCES Abonado (nif,numeroAbonado);
ALTER TABLE Pedido ADD CONSTRAINT FK_Pedido_2 FOREIGN KEY (numeroFactura) REFERENCES Factura (numeroFactura);


ALTER TABLE Preferencia ADD CONSTRAINT FK_Preferencia_0 FOREIGN KEY (idDenominacion) REFERENCES DenominacionOrigen (id);
ALTER TABLE Preferencia ADD CONSTRAINT FK_Preferencia_1 FOREIGN KEY (categoria) REFERENCES Categoria (clave);
ALTER TABLE Preferencia ADD CONSTRAINT FK_Preferencia_2 FOREIGN KEY (nif,numeroAbonado) REFERENCES Abonado (nif,numeroAbonado);

ALTER TABLE Referencia ADD CONSTRAINT FK_Referencia_0 FOREIGN KEY (vinoid) REFERENCES Vino (id);


ALTER TABLE LineaPedido ADD CONSTRAINT FK_LineaPedido_0 FOREIGN KEY (codigo) REFERENCES Referencia (codigo);
ALTER TABLE LineaPedido ADD CONSTRAINT FK_LineaPedido_1 FOREIGN KEY (idLineaCompra) REFERENCES LineaCompra (id);
ALTER TABLE LineaPedido ADD CONSTRAINT FK_LineaPedido_2 FOREIGN KEY (numero) REFERENCES Pedido (numero);


-- ============ Poblaciones fijas: ============

INSERT INTO Categoria VALUES ('A','Del año');
INSERT INTO Categoria VALUES ('C','Crianza');
INSERT INTO Categoria VALUES ('R','Reserva');
INSERT INTO Categoria VALUES ('G','Gran Reserva');

INSERT INTO EstadoFactura VALUES ('E','Emitida');
INSERT INTO EstadoFactura VALUES ('V','Vencida');
INSERT INTO EstadoFactura VALUES ('P','Pagada');

INSERT INTO EstadoPedido VALUES ('P','Pendiente');
INSERT INTO EstadoPedido VALUES ('T','Tramitado');
INSERT INTO EstadoPedido VALUES ('C','Completado');
INSERT INTO EstadoPedido VALUES ('S','Servido');
INSERT INTO EstadoPedido VALUES ('F','Facturado');
INSERT INTO EstadoPedido VALUES ('A','Abonado');

-- ============ Datos de Prueba: ============

INSERT INTO Persona VALUES ('12345678A','Samuel','Alfageme Sainz','C/ Conde Ansurez nº15','+34 665434343','Samuel.Alfageme@gmail.com','ES1020903200500041045040');
INSERT INTO Persona VALUES ('87654321B','Pablo','Carrascal Muñoz','C/ Isabel la Catolica nº16','+34 667984521','PabloCarrascual@carteras.com','ES1020903200500041045041');
INSERT INTO Persona VALUES ('23456789C','Andrea','Escribano García','C/ Juana de Arco nº17','+34 655231211','Escribano@outlook.com','ES1020903200500041045042');
INSERT INTO Persona VALUES ('34567890D','Adrián','de la Rosa','Pza/ los Cisnes 33','+34 666778899','delarosa@mixmail.com','ES1020903200500041045043');
INSERT INTO Persona VALUES ('45678901E','Jorge','Cuadrado Saez','C/ Coker 33','+34 600112233','coke_pucela@gmail.com','ES1020903200500041045044');

INSERT INTO Empleado VALUES ('12345678A','samalfa','1234','2014-12-01','C'); -- Encargado de Contabilidad
INSERT INTO Empleado VALUES ('87654321B','pabcarr','2345','2015-01-25','P'); -- Encargado de Pedidos
INSERT INTO Empleado VALUES ('23456789C','andescr','3456','2010-06-03','A'); -- Encargado de Almacén

INSERT INTO Abonado VALUES ('34567890D',100,'ADRDELA');
INSERT INTO Abonado VALUES ('45678901E',200,'JORCUAD');

-- El sistema comprueba que el abonado no tiene vencido ningún plazo de pago de pedidos anteriores y crea un nuevo pedido

INSERT INTO Factura VALUES (1340,'2015-05-14',136.15,'V',NULL,'123456789000');
INSERT INTO Factura VALUES (1352,'2015-05-27',300.00,'P','2015-06-05','123456789000');

INSERT INTO Pedido(estado, fechaRealizacion, notaEntrega, importe, fechaRecepcion, fechaEntrega, nif, numeroAbonado, numeroFactura) VALUES 
('S','2015-05-10','INCLUIR FACTURA',300.00,'2015-05-25','2015-06-01','45678901E',200,1352),
('P','2015-05-06','ENTREGAR POR LA TARDE',136.15,'2015-05-10',NULL,'34567890D',100,1340);

-- El sistema comprueba que se trata de una referencia válida y que la referencia está disponible, crea una línea de pedido y en el pedido en curso

INSERT INTO DenominacionOrigen VALUES (1,'Ribera de Duero');

INSERT INTO Bodega VALUES (1,'Bodegas Cruz de Alba S.L.','B30789572','C/ Síndico, 4 y 5 - 47350 Quintanilla de Onésimo');

INSERT INTO Vino VALUES (1,'Cruz de Alba',2011,'Cruz de Alba 2011: Mejor Tempranillo del Mundo',1,'A',1);
INSERT INTO Vino VALUES (2,'Cruz de Alba',2010,'15 meses en barrica de roble francés y americano',1,'C',1);

INSERT INTO Referencia VALUES (101,'T',75,14,'T',1);
INSERT INTO Referencia VALUES (102,'T',75,15.25,'F',2);

INSERT INTO Compra (fechaInicioCompra,recibidaCompleta,fechaCompraCompletada,importe,pagada,fechaPago) VALUES 
('2015-05-12','F',NULL,100,'T','2015-05-13');

INSERT INTO LineaCompra(unidades,recibida,fechaRecepcion,idCompra) VALUES 
(5,'F',NULL,1),
(4,'T','2015-05-15',1);

INSERT INTO LineaPedido(unidades,completada,codigo,idLineaCompra,numero) VALUES 
(5,'F',101,NULL,1),
(4,'F',102,NULL,1);

